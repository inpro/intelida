#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/
require "Alignment.pm";


my ($selfFile, $otherFile, $mode) = @ARGV or die "usage: alignmentTimingError.pl <oneAlignment> <anotherAlignment> <mode> \n where mode is either 'match' or 'matchsubst'\n";

my $self = Alignment::newAlignmentFromWavesurferFile($selfFile);
my $other = Alignment::newAlignmentFromWavesurferFile($otherFile);

print $self->computeTimingErrors($other, $mode, "RMSE");
print "\n";
