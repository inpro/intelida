_____________________________________________________________
	  _                                         _           _   
	 (_)_ __  _ __  _ __ ___    _ __  _ __ ___ (_) ___  ___| |_ 
	 | | '_ \| '_ \| '__/ _ \  | '_ \| '__/ _ \| |/ _ \/ __| __|
	 | | | | | |_) | | | (_) | | |_) | | | (_) | |  __/ (__| |_ 
	 |_|_| |_| .__/|_|  \___/  | .__/|_|  \___// |\___|\___|\__|
	         |_|               |_|           |__/               
	_____________________________________________________________

InTELiDa
========

InTELiDa (Incremental Timing Evaluation of Linguistic Data) is an evaluation 
library for all sorts of incremental data, particularly useful for evaluating 
incremental speech recognition (iSR) results.

InTELiDa uses a [simplistic data input format](DATAFORMAT.md) but also handles 
import and export to formats such as Wavesurfer, PRAAT TextGrids and [TEDview](https://bitbucket.org/inpro/tedview/)XML.

The primary evaluation criteria for incremental results are timing and 
stability and InTELiDa additionally computes some standard metrics such as WER. 

Many more details about InTELiDa and the underlying evaluation methodology can 
be found in [Timo Baumann's dissertation](http://www.timobaumann.de/diss/tb-diss-201305231753-publishedversion.pdf).