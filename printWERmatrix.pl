#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;

require "Util.pm";
require "Alignment.pm";
require "TextGrid.pm";
require "SingularAlignmentSequence.pm";
require "NBestAlignmentSequence.pm";

package main;

# usage: ./printWERmatrix.pl data/timo-01.nbinc_reco spiegel dieses teil horizontal und lege es oben direkt an den kopf des tieres an

my $filename = shift or die "need nbinc_reco-file as argument";

# alternativ k�nnte man auch weiter unten den Aufruf von selectBestFinalAsGold() weglassen
my @puregold = @ARGV or die "need your gold transcript as further arguments";

my $seq = NBestAlignmentSequence::newNBestSeqFromFile($filename) or die "error reading file $filename\n";

$seq->selectBestFinalAsGold(@puregold);

my $goldAl = $seq->getGold();
#@puregold = $goldAl->getWords();

# alignment is over when the final word is over
my $end = $goldAl->{labels}->[-1]->{xmax} * 100;

for (my $i = 1; $i <= $end; $i++) {
   my $time = $i * 0.01;
   my $nb = $seq->getNBestListAt($time);
   my @goldWords = $goldAl->getWordsUpTo($time);
   next unless (@goldWords); # we can only start once the first word in the gold alignment starts
   my @WE = $nb->getAllWE(@goldWords);
   #print "$time\t", join("\t", @WE), "\n";
   # use the following two lines if you want WER instead of WE.
    my @WER = map { $_ / scalar @goldWords } @WE;
    print "$time\t", join("\t", @WER), "\n";
}



