#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";

# remove a tier from the textgrid given as argument 
# usage: ./removeTier.pl infile tiername outfile
# prints to STDOUT if no outfile is given

package main;

$#ARGV >= 1 or die "need two or three arguments: filename of textgrid and name of tier, optionally another (or same) filename to save to; will print to STDOUT if no output name is given";
my ($filename, $tiername, $outname) = @ARGV;

my $tg = TextGrid::newTextGridFromFile($filename);
my $tier = $tg->removeAlignmentByName($tiername);
if ($outname) {
	$tg->saveToTextGridFile($outname);
} else {
	print $tg->toTextGridLines();
}
