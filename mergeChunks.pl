#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";

# merge a number of consecutive (according to a file containing cut-marks) wavesurfer files
# into one TextGrid

# Main
# Aufruf mit Pfaden als Argumente

package main;

# HIER NOCH ROBUST MACHEN MIT ARGUMENTPRUEFUNG #
# Pfade

my $fileType;
my $wantedTierName;
my $logFilePath;
my $inpPath;
my $fileEnding;

if(@ARGV > 0) {
	$fileType = shift;
	
	if ($fileType eq "TG") {
	
		if(@ARGV != 4) {
			die "Bad arguments. Please specify the following options: 
			1. TG (TextGrid)
			2. Tiername
			3. Logfiles
			4. Path to the TextGrids
			5. File extension (the ending of the files to be merged)";
		}
		
		$wantedTierName = shift;
		$logFilePath = shift;
		$inpPath = shift;
		$fileEnding = shift;
	
	}
	elsif ($fileType eq "WS") {
		if(@ARGV != 3) {
			die "Bad arguments. Please specify the following options:
			1. WS (WaveSurfer)
			2. Logfiles
			3. Path to WaveSurfer files
			4. File extension (the ending of the files to be merged)";
		}
		$logFilePath = shift;
		$inpPath = shift;
		$fileEnding = shift;
		
	}
	else {
	
		die "Usage for TextGrids: mergeChunks.pl TG [TierName] LOGFILE FILE_EXT
		Usage for WaveSurfer: mergeChunks.pl WS LOGFILE FILE_EXT";
	
	}
	
}

#print "fileType: ",$fileType,"\n";
#print "tierName: ",$wantedTierName,"\n";
#print "logFilePath: ",$logFilePath,"\n";
#print "fileEnding: ",$fileEnding,"\n";

run();

sub run {
	
	open LOG, '<', $logFilePath or die "Cannot open file $logFilePath: $!";
	#open OUTFILE, '>', $outName or die "Could not write to file $outName\n";

	if (substr($inpPath, -1) eq "/") {
	}
	else {
		$inpPath = $inpPath."/";
	}

	my $overallAlignment = Alignment::newEmptyAlignment();
	foreach my $line (<LOG>) 
	{
	# Iterate over the different partial files and add them to one tier
		my $partialFileName;
		my $partialStartTime;
		my $partialEndTime;
		#print "DEBUG: $line\n";
		if($line =~ m/\d+\t(.*?)\t([0-9.]+)\t([0-9.]+)\t[0-9.]+/)
		{
			$partialFileName = $1;
			$partialStartTime = $2;
			$partialEndTime = $3;
		}
		else {
			next;
		}
	
		my $currentAlignment;

		if ($fileType eq "TG")
		{
			my $currentTextGrid = TextGrid::newTextGridFromFile("$inpPath/$partialFileName.TextGrid");
			$currentAlignment = $currentTextGrid->getAlignmentByName($wantedTierName);

		}
		elsif ($fileType eq "WS")
		{
			#print "DEBUG: running wavesurfer merge!\n";
			#print "DEBUG: Working on $participant\n";
			#print "DEBUG: running on \tCutFiles/$partialFileName.$fileEnding\n";
			$currentAlignment = Alignment::newAlignmentFromWavesurferFile("$inpPath/$partialFileName.$fileEnding");
		}
		
		$currentAlignment->crop();
		$currentAlignment->timeShift($partialStartTime);
		
		$overallAlignment->integrateLabelsFromAlignment($currentAlignment, "lax");
	}
	$overallAlignment->crop();
	print $overallAlignment->toWavesurferLines();
}
