#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib";
require "Lexeme.pm";
require "Lexicon.pm";
require "Transcript.pm";


# usage: ./checkAndNormalizeTranscript.pl <dictionary> <transcript>

my ($lexiconFile, $transcriptFile) = @ARGV;

my $lexicon = Lexicon::newFromFile($lexiconFile);
my $transcripts = Transcript::newFromFile($transcriptFile);

my $hasProblems = (1 == 0); # false;

foreach my $id ($transcripts->getIDs()) {
	my $transcript = $transcripts->getTranscript($id);
	my @words = split /\s+/, $transcript;
	map {
		my $word = $_;
		$_ = $lexicon->getNormalizedSpelling($_);
		if (!$_) {
			warn "could not normalize \"$word\" in $id\n";
			$hasProblems = (0 == 0); # true;
			$_ = $word;
		}
	} @words;
	$transcripts->setTranscript($id, join " ", @words);
}
if (!$hasProblems) {
	print "I'm now overwriting your transcript. Farewell...";
	$transcripts->writeToFile($transcriptFile);
} else {
	die "transcript $transcriptFile has words missing from the lexicon";
}
