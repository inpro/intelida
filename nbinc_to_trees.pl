#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

require "NBestAlignmentSequence.pm";
require "Alignment.pm";
require "Lattice.pm";

my ($filename) = @ARGV;
my $seq = NBestAlignmentSequence::newNBestSeqFromFile($filename);

my $count = 0;
for my $nbestList (@{$seq->{nblists}}) {
	my $lat = Lattice::new($nbestList);
	$lat->markGoldPath($nbestList->getNth(0), $seq->getGold());
#	print $lat->toText();
#	print "$filename.$count.dot\n";
	my $outname = $filename . "." . sprintf("%03d", $count) . ".dot";
	open DOT, '>', $outname;
	print DOT $lat->toDot();
	close DOT;
	$count++;
}

__END__

TODO:
+ draw smaller (and potentially nicer) vertices
	-> graphviz documentation
+ adapt linewidth 
	-> graphviz documentation
+ find a way to scale too large images (keeping aspect ratio)
	-> imagemagick documentation

afterwards:
- run dot on the files that were generated:
for file in *.dot; do dot -Tpng $file -o${file%%.dot}.png; done
- find the largest dimension of the images:
-- X coordinate:
identify *.png|cut -d' ' -f3|sed 's/x/ /'|cut -d' ' -f1|sort -n|tail -1
-- Y coordinate:
identify *.png|cut -d' ' -f3|sed 's/x/ /'|cut -d' ' -f2|sort -n|tail -1
--> if your video would end up larger than 2024*2024 you will have problems 
    playing the videos that mencode generates; in this case you may want to 
    resize the images that are too large instead of extending:
TODOOOOO: for file in *.png; do convert $file -background white -gravity west -extent 2024x1280 ${file%%.png}.l.png; done
- extend images so that they're all of the same size.
for file in *.png; do convert $file -background white -gravity west -extent 2024x1280 ${file%%.png}.ext.png; done
- generate a video out of the images:
mencoder "mf://*.png" -mf type=png:fps=100 -ovc lavc -o output.avi
- enjoy your new video:
mplayer output.avi
