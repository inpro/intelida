#!/usr/bin/perl
# Copyright (C) 2010 Timo Baumann
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Audio::Wav;
use Audio::Wav::Read;

#usage: audio-duration.pl path-or-file1 path-or-file2 ...

my @files;
for my $arg (@ARGV) {
	my $findresult = `find $arg`;
	push @files, grep /\.wav$/, split "\n", $findresult;
}
#print join " ", @files;
my $duration = 0.0;
my $wav = new Audio::Wav;
for my $file (@files) {
	my $read = $wav->read($file);
	$read->{handle}->close(); # workaround to make sure file-handles are closed
	$duration += $read->length_seconds();
	$read = undef;
}
# convert to something readable
my $readableDuration = "";
if ($duration > 600) {
	my $seconds = int($duration + .5);
	my $minutes = int($duration / 60);
	$seconds -= $minutes * 60;
	my $hours = int($minutes / 60);
	$minutes -= $hours * 60;
	$readableDuration = "(" . ($hours > 0 ? "$hours:" : "") . "$minutes'$seconds\") ";
}
print "$duration seconds ", $readableDuration, "in ", ($#files + 1), " wave files.\n";
