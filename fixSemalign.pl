#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

require "Util.pm";
require "TextGrid.pm";
require "Wave.pm";
use utf8;
binmode STDIN, ':utf8';
binmode STDOUT, ':utf8';
use open qw(:std :utf8);

my %turns; 

my $filename = $ARGV[0] or die "I need a SemAlign-Text file as first argument";
$filename =~ m/(D[12])\_\d+.txt/ or die "that's a weird filename: $filename";
my $day = $1;

my $tgname = $filename;
$tgname =~ s/.txt$/.TextGrid/;
my $tg = TextGrid::newTextGridFromFile($tgname);

my @labels = $tg->getAlignmentByName('IG')->getLabels();
my $currentlabel;

my $globaloffset = undef;
# read the file information into the turns hash
while (my $line = <>) {
	chomp $line;
	chomp $line;
	if ($line =~ m/^----/ || $line =~ m/^#/ || $line =~ m/^agent_connect to Facilitator/) {
		print "$line\n";
	} else {
		my ($file, $turn, $key, $val) = $line =~ m/(\d+)\.(\d+)\:(.+?)\:\s*(.*)/ or die "could not parse: $line\n";
		if ($key eq 'utt') {
			my $cleanval = $val;
			$cleanval =~ s/ \| / /g;
			$currentlabel = $labels[$turn - 1];
#			$currentlabel->{text} eq $cleanval or 
#				die "doesn't match: in $day:$file:$turn:\n$currentlabel->{text}\n$cleanval\n\n";
		}
		if ($key eq 'xmin' || $key eq 'xmax') {
			my $offset = $val - $currentlabel->{$key};
			if (defined $globaloffset) {
				my $squarederror = ($globaloffset - $offset) * ($globaloffset - $offset);
				warn "$filename:$turn: $key: $squarederror" if ($squarederror > 0.02);
			} else {
				$globaloffset = $offset;
				print STDERR "correcting by $offset\n";
			}
#			$val = $currentlabel->{$key};
		}
		print "$file.$turn:$key:\t$val\n";
	 }
}

$tg->saveToTextGridFile("$tgname.NEW");

