#!/usr/bin/perl

use strict;
use warnings;

# get at hypothesis revision times, which can be used to estimate
# hypothesis stability

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

require "SingularAlignmentSequence.pm";
use Data::Dumper;
require "Label.pm";
use Carp;

my $l = Label::new(0, 1, '<sil>');
confess unless ($l->isSilent());
confess unless (Label::isSilentText($l->{text}));

my $seq;

#$seq = SingularAlignmentSequence::newSeqFromFile("DE_1234.inc_reco");

#my $file = "/home/timo/uni/projekte/diss/iASR/verbmobil/g392acn1_029_ALJ.standard.inc_reco";
foreach my $file (@ARGV) {
	$seq = SingularAlignmentSequence::newSeqFromFile($file);
	print "$file:\n" . Dumper $seq->getHypothesisAges();
}


__END__
algorithm can be as follows:
- make a hash to count individual addTimes
- keep a list of @addTimes
- iterate over @alignments and find @IUEdits relative to previous alignment
- if add: add time of current alignment to @addTimes
- if revoke: remove last time from @addTimes and increase %addTimesCount
