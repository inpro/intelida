#!/usr/bin/perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

require "TextGrid.pm";
require "Alignment.pm";

my $tg = TextGrid::newTextGridFromTextGridFile("iustructure.test.TextGrid");
print $tg->toMaryXMLLines("iustructure.test.lf0");
