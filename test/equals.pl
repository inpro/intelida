#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

#require "Alignment.pm";
require "Label.pm";

my $fileRoot = 'equals';

my $l1 = Label::newLabelFromWavesurferLine('0	1	a');
die "identity should be equal" unless $l1->equals($l1);
my $l2 = Label::newLabelFromWavesurferLine('0	1	a');
die "same data should be equal" unless $l1->equals($l2);
die "same data should be equal" unless $l2->equals($l1);
my $l3 = Label::newLabelFromWavesurferLine('0	2	a');
die "different times should NOT be equal" if $l1->equals($l3);
die "different times should NOT be equal" if $l3->equals($l1);
die "different times should be equal with 'lax'" unless $l1->equals($l3, 'lax');
die "different times should be equal with 'lax'" unless $l3->equals($l1, 'lax');
my $l4 = Label::newLabelFromWavesurferLine('0	2	b');
die "different text should NOT be equal" if $l1->equals($l4);
die "different text should NOT be equal" if $l1->equals($l4, 'lax');
die "different text should NOT be equal" if $l4->equals($l1);
die "different text should NOT be equal" if $l4->equals($l1, 'lax');


