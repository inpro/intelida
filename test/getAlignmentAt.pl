#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "SingularAlignmentSequence.pm";


my $fileRoot = 'getAlignmentAt';

my $seq = SingularAlignmentSequence::newSeqFromFile("$fileRoot.test");

open OUT, '>', "$fileRoot.output";

print OUT $seq->getAlignmentAt(0.0)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.42)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.43)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.45)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.46)->toSphinxLines();
print OUT "\n";

$seq->cropSequence();

print OUT $seq->getAlignmentAt(0.0)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.42)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.43)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.45)->toSphinxLines();
print OUT "\n";
print OUT $seq->getAlignmentAt(0.46)->toSphinxLines();
print OUT "\n";

close OUT;

print `diff $fileRoot.output $fileRoot.ref`
