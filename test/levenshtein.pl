#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "Util.pm";
require "MyBrew.pm";

my $fileRoot = 'levenshtein';

my @a = ('one', 'two', 'three', 'four');
my @b = ('one', 'three', 'four');
open OUT, '>', "$fileRoot.output";

print OUT Util::levenshtein(\@a, \@b);
print OUT "\n";

my $out = MyBrew::distance(\@a, \@b, {-output => 'edits'});

print OUT join ", ", @{$out};
print OUT "\n";

print `diff $fileRoot.output $fileRoot.ref`
