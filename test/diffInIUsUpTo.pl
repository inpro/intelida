#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "Alignment.pm";
require "SingularAlignmentSequence.pm";

my $fileRoot = 'diffInIUsUpTo';

my $al0 = Alignment::newEmptyAlignment();

my $al1 = Alignment::newAlignmentFromWavesurferLines(
'0	1	<sil>',
'1	2	a',
'2	3	b');

my $al2 = Alignment::newAlignmentFromWavesurferLines(
'0	1	<sil>',
'1	2	a',
'2	3	b',
'3	5	<sil>');

my $al3 = Alignment::newAlignmentFromWavesurferLines(
'0	1	<sil>',
'1	2	a',
'2	3	b',
'3	4	c');

my $al4 = Alignment::newAlignmentFromWavesurferLines(
'0	1	<sil>',
'1	2	a',
'2	3	d',
'3	4	e');

print join ", ", map { $_->toString() } $al1->diffInIUEdits($al0, 0); # "add(a), add(b)" should be printed
print "\n";
print join ", ", map { $_->toString() } $al1->diffInIUEdits($al2, 0); # nothing should be printed
print "\n";
print join ", ", map { $_->toString() } $al1->diffInIUEdits($al2, 0); # nothing should be printed
print "\n";
print join ", ", map { $_->toString() } $al1->diffInIUEdits($al2, 9999); # nothing should be printed
print "\n";
print join ", ", map { $_->toString() } $al1->diffInIUEdits($al3, 9999); # "revoke(c)" should be printed
print "\n";
print join ", ", map { $_->toString() } $al3->diffInIUEdits($al1, 9999); # "add(c)" should be printed
print "\n";
print join ", ", map { $_->toString() } $al3->diffInIUEdits($al4, 9999); # "revoke(e), revoke(d), add(b), add(c)" should be printed
print "\n";
print join ", ", map { $_->toString() } $al3->diffInIUEdits($al4, 9999, '', 'subst'); # "revoke(e), revoke(d), add(b), add(c)" should be printed
print "\n";

#my $seq = SingularAlignmentSequence::newSeqFromFile("$fileRoot.test");

#$al1 = $seq->getAlignmentAt(0.78);
#print $al1->toSphinxLines();
#$al2 = $seq->getAlignmentAt(0.79);
#print $al2->toSphinxLines();
#print join ", ", map { $_->toString() } $al2->diffInIUEdits($al1, 9999);
#print "\n";
#$al3 = $seq->getAlignmentAt(0.88);
#print $al3->toSphinxLines();
#print join ", ", map { $_->toString() } $al3->diffInIUEdits($al2, 9999);
#print "\n";
#print join ", ", map { $_->toString() } $al3->diffInIUEdits($al2, 9999, '', 'subst');
#print "\n";

