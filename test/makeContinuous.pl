#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "SingularAlignmentSequence.pm";

my $fileRoot = 'makeContinuous';


my $seq = SingularAlignmentSequence::newSeqFromFile("$fileRoot.test");

$seq->makeContinuous();

$seq->saveToSphinxFile("$fileRoot.output");

print `diff $fileRoot.output $fileRoot.ref`
