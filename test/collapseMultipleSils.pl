#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

require "Alignment.pm";

my $alignment = Alignment::newAlignmentFromWavesurferLines(
'0.0	0.1	<sil>',
'0.1	0.2	<sil>',
'0.2	0.3	_',
'0.3	0.4	_',
'0.4	0.5	<sil>',
'0.5	0.6	lala',
'0.6	0.7	<sil>');

print join "", $alignment->toWavesurferLines();
$alignment->collapseMultipleSils();
print join "", $alignment->toWavesurferLines();

