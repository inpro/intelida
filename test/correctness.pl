#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "SingularAlignmentSequence.pm";

my %EO;
my %CORR;

foreach my $file (@ARGV) {
	my $seq = SingularAlignmentSequence::newSeqFromFile($file);
	$seq->fixedLagSequence(0.09);
#	$seq->smoothSequence(10);
	my %fileEO = $seq->getEditOverheadHash();
	foreach my $key (keys %fileEO) {
		$EO{$key} += $fileEO{$key};
	}
	$EO{EO} = ($EO{changes} - $EO{words}) / $EO{changes};
	my %fileCORR = $seq->getCorrectnessHash(0.09);
	foreach my $key (keys %fileCORR) {
		$CORR{$key} += $fileCORR{$key};
	}

}
foreach my $key (sort keys %EO) {
	print "$key:\t$EO{$key}\n";
}
print "\n";
foreach my $key (sort keys %CORR) {
	print "$key:\t$CORR{$key}\n";
}
print "strict correctness:\t" . ($CORR{strictCorrect} / $CORR{total}) . "\n";
print "prefix correctness:\t" . ($CORR{prefixCorrect} / $CORR{total}) . "\n";
print "fair correctness:\t" . ($CORR{fairCorrect} / $CORR{total}) . "\n";
print "\n";
#printOutput($seq, 0);
#printOutput($seq, 0.1);


sub printOutput {
	my ($seq, $fairness) = @_;
	my %EO = $seq->getEditOverheadHash();
	foreach my $key (sort keys %EO) {
		print "$key:\t$EO{$key}\n";
	}
	print "\n";

	my %CORR = $seq->getCorrectnessHash($fairness);
	foreach my $key (sort keys %CORR) {
		print "$key:\t$CORR{$key}\n";
	}
	print "\n";
}

