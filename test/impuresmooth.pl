#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from ../lib/

use strict;
use warnings;

require "SingularAlignmentSequence.pm";

my $fileRoot = '/home/timo/inpro/Scripts/test/impuresmooth';

my $seq;
# $seq = SingularAlignmentSequence::newSeqFromFile("$fileRoot.input");

#$seq->impureSmoothSequence(5, 1);
#$seq->smoothSequence(5);
#$seq->makeContinuous();
#$seq->makeSparse();

#print $seq->toSphinxLines();

$seq = SingularAlignmentSequence::newSeqFromFile("/home/timo/uni/projekte/diss/iASR/verbmobil/g392acn1_029_ALJ.standard.inc_reco");
#$seq->{alignments} = [@{$seq->{alignments}}[0..200]];
$seq->impureSmoothSequence(13, 1);
$seq->getFODelays();
$seq->getFDDelays();
$seq->getEditOverheadHash();
#print $seq->toSphinxLines();

