#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib"; # import libraries from lib/
require "SingularAlignmentSequence.pm";

foreach my $file (@ARGV) {
	print "$file\n";
	my $seq = SingularAlignmentSequence::newSeqFromFile($file);
#	$seq->makeSparse();
#	$seq->makeContinuous();
#	print $seq->toSphinxLines();
	$seq->fixedLagSequence(0.50);
#	$seq->smoothSequence(10);
#	print $seq->toSphinxLines();
	$seq->checkFOFD();
}
