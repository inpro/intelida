#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;

require "NBestAlignmentSequence.pm";
require "SingularAlignmentSequence.pm";
require "Alignment.pm";
require "Util.pm";
require "TextGrid.pm";
require "Util.pm";

package INshell;
require Term::Shell;
use base qw(Term::Shell);

my $lastfilename = '';
my $hasChanges = Util->FALSE;
my $seq;
my $singSeq;


sub new {
    my $cls = shift;
    my $o = bless {
        term    => eval {
            # Term::ReadKey throws ugliness all over the place if we're not
            # running in a terminal, which we aren't during "make test", at
            # least on FreeBSD. Suppress warnings here.
            local $SIG{__WARN__} = sub { };
            Term::ReadLine->new('shell', *STDIN, *STDOUT);
        } || undef,
    }, ref($cls) || $cls;

    # Set up the API hash:
    $o->{command} = {};
    $o->{API} = {
        args            => \@_,
        case_ignore     => ($^O eq 'MSWin32' ? 1 : 0),
        check_idle      => 0,   # changing this isn't supported
        class           => $cls,
        command         => $o->{command},
        cmd             => $o->{command}, # shorthand
        match_uniq      => 1,
        pager           => $ENV{PAGER} || 'internal',
        readline        => eval { $o->{term}->ReadLine } || 'none',
        script          => (caller(0))[1],
        version         => 0.02,
    };

    # Note: the rl_completion_function doesn't pass an object as the first
    # argument, so we have to use a closure. This has the unfortunate effect
    # of preventing two instances of Term::ReadLine from coexisting.
    my $completion_handler = sub {
        $o->rl_complete(@_);
    };
    if ($o->{API}{readline} eq 'Term::ReadLine::Gnu') {
        my $attribs = $o->{term}->Attribs;
        $attribs->{completion_function} = $completion_handler;
    }
    elsif ($o->{API}{readline} eq 'Term::ReadLine::Perl') {
        $readline::rl_completion_function = 
        $readline::rl_completion_function = $completion_handler;
    }
    $o->find_handlers;
    $o->init;
    $o;
}


sub prompt_str { "in> " }


########## directory handling commands ##########

sub smry_ls { "list the directory" }
sub run_ls {
	my ($o, @args) = @_;
	system 'ls', @args;
}

sub smry_cd { "change directory" }
sub run_cd {
	my ($o, $dir) = @_;
	($dir) ? chdir $dir : chdir;
}

sub smry_pwd { "print working directory" }
sub run_pwd {
	my ($o, $dir) = @_;
	system 'pwd';
}

########## file handling commands ##########

sub smry_load { "load a file" }
sub help_load {
<<'END';
load  <filename> 	load a file
END
}

sub run_load {
	my ($o, $filename) = @_;
	if (!defined $filename) {
		run_new();
	} else {
		if ($hasChanges) {
			print "Discarding changes. I can't yet save automatically. Sorry for that.\n";
		}
		$seq = NBestAlignmentSequence::newNBestSeqFromFile($filename);
		$lastfilename = $filename;
		$hasChanges = Util->FALSE;
	}
	# initialize with 1-best hypotheses
	run_toSing(); 
}

sub comp_load {
	my @comps = glob "*";
	return @comps;
}

sub smry_save { "save to file" }
sub help_save {
<<'END';
save  <filename>	save current singularAlignmentSequence to file
END
}

sub run_save {
	my ($o, $filename) = @_;
	print "NOTE: I'm saving your singularAlignmentSequence. Please extend me to also support NBestSequences. Thank you!\n" if (Util->DEBUG > 0);
	$singSeq->saveToSphinxFile($filename);
}

sub smry_show { "show alSeq on console" }
sub run_show {
	my ($o) = @_;
	print $singSeq->toSphinxLines();
}

########## Nbest->1best ##########

sub smry_limitN { "limit maximum N of n-best lists" }

sub run_limitN {
	my ($o, $N) = @_;
	$seq->limitN($N);
}

sub smry_toSing { "convert to singular alignment sequence by taking the N'th best hypothesis from each n-best list" }
sub help_toSing {
<<'END';
toSing  <N>        convert an NBestAlignmentSequence to a SingularAlignmentSequence 
                   by taking the N'th best hypothesis from each n-best list
                   N defaults to 0, which results in the best hypotheses being selected

END
}
sub run_toSing {
	my ($o, $N) = @_;
	$singSeq = $seq->toSingularAlignmentSequence($N);
}

########## manipulate sequence ##########

sub smry_crop { "crop the sequence to the active part of recognition (where the
actual changes are happening" }
sub run_crop { 
	$singSeq->cropSequence();
}

sub smry_continuous { "make the sequence continuous, i.e. insert hypotheses if
some frames have no hypothesis" }
sub run_continuous {
	$singSeq->makeContinuous();
}

sub smry_sparse { "remove redundant (with regard to IU writing) alignments from the alignment sequence" }
sub run_sparse {
	my ($o) = @_;
	$singSeq->makeSparse();
}

sub smry_fixedLag { "use fixed-lag hypothesis filtering (cmp NAACL paper section 4.1)" }
sub help_fixedLag {
<<'END';
fixedLag  <t>      use fixed-lag hypothesis filtering (cmp NAACL paper section 4.1)
                   hypotheses are only considered up to a fixed-lag t
                   t is measured in seconds
END
}
sub run_fixedLag {
	my ($o, $lag) = @_;
	$singSeq->fixedLagSequence($lag);
}

sub smry_smoothing { "use smoothing hypothesis filtering (cmp NAACL paper section 4.2)" }
sub run_smoothing {
	my ($o, $smoothing) = @_;
	$singSeq->smoothSequence($smoothing);
}

sub smry_onlyrecent { "only keep recent labels (parameter in seconds) from the alignments; helpful when showing long alignment sequences in TEDview" }
sub run_onlyrecent {
	my ($o, $recency) = @_;
	$singSeq->onlyRecent($recency);
}

sub smry_timeshift { "timeshift all labels (optionally/TBD: also alignment times)" }
sub run_timeshift {
	my ($o, $shift) = @_;
	foreach my $alignment ($singSeq->getAlignments()) {
		$alignment->timeShift($shift);
	}
}

sub smry_TEDview { "show alSeq in TEDview" }
sub help_TEDview {
<<'END';
TEDview <orig> <port>	show alSeq in TEDview (listening on <port>), use pane called <orig>
		        <port> defaults to 2000 (TEDview's default port) and <orig> to 'INTELIDA_asr'
END
}

sub run_TEDview { 
#	my ($o, $orig) = @_;
	my ($o, $orig, $port) = @_;
	$port = 2000 unless ($port);
#	$singSeq->toTED($orig);
	$singSeq->getFDDelays();
	$singSeq->toTED($orig, '127.0.0.1', $port);
}

sub smry_svg { "output incremental sequence as SVG file" }
sub help_svg {
<<'END';
SVG <file>    write graphical representation to <file> in SVG format.
END
}

sub run_svg {
	my ($o, $file) = @_;
	if (defined $file) {
		open SVG, '>', $file or warn "could not write to $file\n" and return;
		print SVG $singSeq->toSVG()->xmlify();
		close SVG;
	} else {
		print $singSeq->toSVG()->xmlify();
	}
}

sub smry_last { "show the last (final) hypothesis from the singular sequence" }
sub run_last {
	print $singSeq->getLastAlignment()->toWavesurferLines();
}

sub smry_evalEO { "show incremental evaluation metric: Edit Overhead" }
sub help_evalEO { "computes and summarises edit overhead-related metrics" }
sub run_evalEO {
	my %file_eoHash = $singSeq->getEditOverheadHash();
	while (my ($stat, $value) = each %file_eoHash) {
		print "$stat: $value\n";
	}
}

sub smry_evalWER { "show word error rate" }
sub help_evalWER { "evalWER [<transcript>] compute and show WER, against the transcript or a gold alignment if available" }
sub run_evalWER {
	my ($o, @transcript) = @_;
	my $transcript = (@transcript) ? 
				  join " ", @transcript 
				: undef; 
	my @edits = @{$singSeq->getWEList($transcript)};
	my $WE = scalar grep { m/INS|DEL|SUBST/ } @edits;
	my $goldWordCount = (@transcript) ? 
				  scalar split " ", $transcript 
				: scalar $singSeq->getGold()->getWords();
	print "errors = $WE\ngold words = $goldWordCount\nWER = " . ($WE / $goldWordCount) . "\n";
}

sub smry_evalFO { "print the first occurrence timing metric" }
sub help_evalFO { "evalFO [ all | match | matchsubst ] compute FO for all, matching or matching/substituted words" }
use Data::Dumper;
sub run_evalFO {
	my ($o, $strategy) = @_;
	$strategy = "all" unless (defined $strategy);
	print Data::Dumper::Dumper($singSeq->getFODelaysWER($strategy));
}

sub smry_evalFD { "print the final decision timing metric" }
sub help_evalFD { "evalFD [ all | match | matchsubst ] compute FD for all, matching or matching/substituted words" }
sub run_evalFD {
	my ($o, $strategy) = @_;
	$strategy = "all" unless (defined $strategy);
	print Data::Dumper::Dumper($singSeq->getFDDelaysWER($strategy));
}

sub smry_addGold { "add a gold alignment at the end of the opened file" }
sub help_addGold { "addGold <alignmentFile> [<tier>] the alignment file in Wavesurfer format (or TextGrid with a given tier)" }
sub run_addGold {
	my ($o, $gold, $tier) = @_;
	if ($singSeq->getGold != $singSeq->getLastAlignment) {
		print "This alignment sequence already has a gold alignment!\n";
		return;
	}
	my $alignment;
	if (defined $tier) {
		my $tg = TextGrid::newTextGridFromFile($gold);
		$alignment = $tg->getAlignmentByName($tier);
	} else {
		$alignment = Alignment::newAlignmentFromWavesurferFile($gold); 
	}
	$alignment->{gold} = (1 == 1); # true
	$singSeq->{gold} = $alignment;
}

########## main package ##########

package main;

my $myshell = INshell->new;
if (@ARGV) {
	foreach my $arg (@ARGV) {
		$myshell->cmd($arg);
	}
} else {
	STDIN->clearerr();
	$myshell->cmdloop;
}
