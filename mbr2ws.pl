#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";
require "Alignment.pm";
require "Label.pm";

# convert mbrola input to an alignment in wavesurfer label file format
# usage: cat somefile.mbr | ./mbr2ws.pl > same.TextGrid
# the optional parameter switches between word or phone output
# notice that the mbr-file must be manually annotated for word output
# annotation format: add a comment (start line with ";") with the word
# that starts at this position. 
# after the last word or before pauses add an empty comment

package main;

my $TIME_SCALE = 0.001; # mbrola counts milliseconds, TextGrid counts seconds

my $curWord = "";
my $wordTime = 0.0;
my $curTime = 0.0;
my $wordAlignment = Alignment::new("words", 0, 0, [], "lax");
my $phoneAlignment = Alignment::new("phones", 0, 0, [], "lax");
while (<STDIN>) {
	if (m/^; *(.*)$/) { # word-marker-comments
		if ($wordTime != $curTime) {
			$wordAlignment->addLabel(Label::new($wordTime, $curTime, $curWord));
		}
		$curWord = $1;
		$wordTime = $curTime;
	} elsif (m/^#/) { # ignore boundaries
	} elsif (m/^$/) { # ignore blank lines
	} elsif (m/^_ (\d+)/) { # silence
		$curTime += $1 * $TIME_SCALE;
	} elsif (m/^(..?\:?) (\d+)/) {
		my $phone = $1;
		my $duration = $2 * $TIME_SCALE;
		$phoneAlignment->addLabel(Label::new($curTime, $curTime + $duration, $phone));
		$curTime += $duration;
	} else {
		print STDERR "cannot parse line: $_\n";
	}
}
if (($curWord ne "") && ($wordTime < $curTime)) {
	$wordAlignment->addLabel(Label::new($wordTime, $curTime, $curWord));
}
$wordAlignment->removeEmptyLabels();
$phoneAlignment->removeEmptyLabels();
my $tg = TextGrid::new(0, 0, [$wordAlignment, $phoneAlignment], 'lax');
print join "", $tg->toTextGridLines();
