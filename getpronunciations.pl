#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib";
require "TextGrid.pm";
require "Lexeme.pm";
require "Lexicon.pm";

my $usage = "usage: getpronunciations.pl [-c canonicalLexicon] \
[-w wordAlignment] \
[-p phoneAlignment] \
[-ß listwithtranslations] \
file1 ...\n";

my $wordAlignmentExp = "words|ORT:";
my $phoneAlignmentExp = "phones|MAU:";

if ($#ARGV < 0) {
	print STDERR $usage;
	exit;
}

my $lexicon = Lexicon::new();

my %translations;

while ($ARGV[0] =~ m/-[wpcß]/) {
	my $opt = shift @ARGV;
	if ($opt eq '-w') {
		$wordAlignmentExp = shift @ARGV;
	} elsif ($opt eq '-p') {
		$phoneAlignmentExp = shift @ARGV;
	} elsif ($opt eq '-c') {
		$lexicon->openCanonicalLexicon(shift @ARGV);
	} elsif ($opt eq '-ß') {
		readTranslations(shift @ARGV);
	} else {
		print STDERR $usage;
	}	
}

foreach my $file (@ARGV) {
	my $tg= TextGrid::newTextGridFromFile($file);
	my $wordAlignment;
	my $segAlignment;
	$wordAlignment = $tg->getAlignmentByName($wordAlignmentExp);
	$segAlignment = $tg->getAlignmentByName($phoneAlignmentExp);
	die "Failed in file $file" unless ((defined $wordAlignment) && (defined $segAlignment));
	foreach my $word ($wordAlignment->getLabels()) {
		next if ($word->isSilent());
		my $segsOfWord = $segAlignment->getSpan($word->{xmin}, $word->{xmax}, 'strict');
		my $spelling = $word->{text};
		if (exists $translations{$spelling}) {
			$spelling = $translations{$spelling};
		}
		$spelling =~ s/\<//; 
		$spelling =~ s/\>//;
		$lexicon->add($spelling, $segsOfWord->getLabels());
	}
}
#$lexicon->outputByFrequency();
$lexicon->outputOnlyMostCommonPronunciations();
#$lexicon->outputByLexicalOrder();

sub readTranslations {
	my $filename = shift;
	open TRL, '<:encoding(utf8)', $filename or die "Could not open $filename: $!";
	while (my $line = <TRL>) {
		chomp $line; chomp $line;
		$line =~ m/^(.+)\t(.+)$/ or die "illegal format $line in $filename";
		my ($old, $new) = ($1, $2);
		$translations{$old} = $new;
	}
}
