#!/usr/bin/perl -I/home/timo/perl-lib/lib/perl/5.8.8/

use strict;
use warnings;
#use utf8;

use FindBin;

# TODO:
# make initial user data required on registration configurable
# implement randomized stimuli (and abort after maxStimuli
# make experiment types configurable
# implement storing information in files

use CGI qw/:standard/;
use CGI::Carp qw/fatalsToBrowser/; # to write to the Apache log and to the browser
use Data::UUID; # for user IDs; need to get this from CPAN as it's not part of the default installation

# use this to localize the script
my %localizationData = ( 
	greeting => 'Liebe Teilnehmerin, lieber Teilnehmer, <br>
vielen Dank, dass Sie sich f&uuml;r dieses Experiment interessieren. <br>
Bitte geben Sie Ihren Namen (oder Pseudonym) sowie ihr Alter an. <br>
Dr&uuml;cken Sie dann bitte auf "weiter".',
	expStart => "Auf der n&auml;chsten Seite beginnen die eigentlichen Fragen.",
	expDone => "Das war alles. Vielen Dank, dass Sie an unserer Studie teilgenommen haben.",
	name => "Name", 
	age => "Alter",
	likert5_5 => "trifft stark zu",
	likert5_4 => "trifft etwas zu",
	likert5_3 => "neutral",
	likert5_2 => "trifft kaum zu",
	likert5_1 => "trifft &uuml;berhaupt nicht zu",
	'next' => "weiter",
);

my $DEBUG = (1 == 0); # true

my $cgi = CGI->new();
my $variables = $cgi->Vars();
my $userID = $cgi->cookie(-name => 'userID');
# copy userID to variables for convenience
if (!defined $userID || $userID eq '') {
	$userID = $variables->{'userID'};
} else {
	$variables->{'userID'} = $userID; 
}
my $newUserCookie = cookie(
	-name => 'userID',
	-value => $userID,
	-expires => '+3M');
# handle information that was just submitted
if ($cgi->request_method() eq 'POST') {
	handlePost();
}
# start output for the next page to be shown
print header(-cookie => $newUserCookie);
print start_html(-title => 'PWebExperimenter');
print start_form(-method => 'POST');
if (!exists $variables->{'userID'} || $variables->{'userID'} eq '') { 
	# we first register the user
	handleRegistration();
} elsif (!exists $variables->{'expID'}) { 
	# then select an experiment
	handleExperimentAssignment();
} elsif ($variables->{'state'} eq 'expSetup') {
	handleExperimentSetup();
} else { # both userID and expID exist, we can start the experiment
	handleExperimentStimulus();
}
print end_form();
print end_html();

#  END MAIN  #
# SUBs below #

# the form variable 'state' always stores the post data's transmitted information
# that may have to be stored in the user or experiment database
sub handlePost {
	if ($variables->{'state'} eq 'userInfo') {
		my @userKeys = grep m/^user/, keys %{$variables};
		carp join ", ", @userKeys;
		carp "adding entries to key-value-pair store: \n\t"
		   . join "\n\t", map { "$_:$variables->{$_}" } @userKeys;
		carp "new user with ID $variables->{userID} named $variables->{userNAME} who is $variables->{userAGE} years old.";
	} elsif ($variables->{'state'} eq 'expSetup') {
		# nothing to do, expID variable should be automatically set from the form
	} elsif ($variables->{'state'} eq 'expStimulus') {
		# record stimulus rating
		my @expKeys = sort grep m/^exp/, keys %{$variables};
		unshift @expKeys, 'userID';
		carp "adding entries to key-value-pair store: \n\t"
		   . join "\n\t", map { "$_:$variables->{$_}" } @expKeys;
	}
}

# in the registration phase, users are asked some questions (answers are stored in handlePost())
sub handleRegistration {
	print p($localizationData{greeting});
	print table({-border => '0'},
		Tr({}, [
			td([$localizationData{name} . ": ", 
			    textfield(-name => 'userNAME', -size => 15)]),
			td([$localizationData{age} . ": ",
			    textfield(-name => 'userAGE', -size => 3)]),
			td(['',submit({-ID => 'next', -name => $localizationData{'next'}})])
		]));
	print hidden('state', 'userInfo');
	print hidden('userID', (defined $userID) ? $userID->value : newID());
}

sub handleExperimentAssignment {
	print p("You've been assigned to one of our experiments. Please press 'next'.");
	# TODO: choose among experiments, present experiment description to user,
	# set expID and make user press "next"
	# no need to set userID, we take it from the cookie
	param('state', 'expSetup');
	print hidden('state', 'expSetup');
	print hidden('expID', 'exp1');
	print submit({-ID => 'next', -name => $localizationData{'next'}});
}

sub handleExperimentSetup {
	my $stage = $variables->{'expSetupStage'} || 1;
	print p("$variables->{expID} in stage $stage") if ($DEBUG);
	my $exp = Experiment::getExperiment($variables->{expID});
	my $text = $exp->getSetupInfo($stage);
	my $state = ($text eq '') ? 'expRunning' : 'expSetup';
	if ($text eq '') {
		$text = p($localizationData{'expStart'});
	}
	print $text;
	param('state', $state);
	print hidden('state');
	$stage++;
	param('expSetupStage', $stage);
	print hidden('expSetupStage');
	print hidden('expID');
	print p(submit({-ID => 'next', -name => $localizationData{'next'}}));
}

sub handleExperimentStimulus {
	my $stimulusCount = $variables->{'expStimulusCount'} || 0;
	param('expStimulusCount', $stimulusCount + 1);
	my $exp = Experiment::getExperiment($variables->{expID});
	my $text = $exp->getStimulus($stimulusCount);
	if ($text ne '') {
		print $text;
		print $exp->getQuestions();
		# no need to set userID, we can get it from the cookie
#		print hidden('state', 'expStimulus');
		param('state', 'expStimulus');
		print hidden('state');
		print hidden('expID');
		print hidden('expStimulusCount');
		print '<script><!--
function donePlaying() {document.getElementById("next").disabled=false;}//--></script>';
		print p(submit({-disabled => 'disabled', -ID => 'next', -name => $localizationData{'next'}}));
	} else {
		print p($localizationData{expDone});
	}
}

sub newID {
	return new Data::UUID->create_str();
#	return "not quite random";
}

package UserTable;

my $userFile = "user.kvps";

# interface to the experiment description files
package Experiment;
use CGI qw/:standard/;

sub getExperiment {
	my ($expID) = @_;
	my $self = { expID => $expID };
	open EXPINFO, '<', $expID;
	while (my $line = <EXPINFO>) {
		next if ($line =~ m/^#/);
		chomp $line; chomp $line;
		my ($key, $value) = split /\t/, $line;
		$self->{$key} = $value;
	}
	return bless $self;
}

sub getSetupInfo {
	my ($self, $stage) = @_;
	if (defined $self->{"setup$stage"}) {
		return $self->{"setup$stage"};
	} else {
		return '';
	}
}

sub getStimulus {
	my ($self, $i) = @_;
	if (defined $self->{orderOfStimuli} && $self->{orderOfStimuli} eq 'random') {
		return '' if ($i > $self->{maxStimuli});
		return $self->getRandomStimulus();
	} else {
		return $self->getOrderedStimulus($i);
	}
}

sub getRandomStimulus {
	my $self = $_[0];
	my $max = scalar grep m/^stimulus/, keys %{$self};
	return $self->getOrderedStimulus(int(rand($max)));
}

sub getOrderedStimulus {
	my ($self, $i) = @_;
	print "presenting stimulus $i" if ($DEBUG);
	$i++; # stimulus counts start at 1
	my $stimulus = $self->{"stimulus$i"};
	if (defined $stimulus) {
		my ($name, $type, @remainder) = split ":", $stimulus;
		param('expStimulusName', $name);
		print hidden('expName');
		if ($type eq 'audio') {
			my ($filename) = join ":", @remainder;
			param('expFile', $filename);
			print hidden('expFile');
			my $text = p("<audio preload autoplay src='$filename' onended='donePlaying();'/>");
			return $text;
		} else {
			die "I only support audio stimuli for now";
		}
	} else {
		return '';
	}
}

sub getQuestions {
	my ($self) = @_;
	my @questions = sort grep m/^question/, keys %{$self};
	my $text = '';
	foreach my $question (@questions) {
		my ($name, $type, @remainder) = split ":", $self->{$question};
		if ($type eq 'likert5') {
			my ($description) = @remainder;
			$text .= p($description);
			$text .= '\n<p>\n';
			foreach my $num ((5, 4, 3, 2, 1)) {
				$text .= "<span style='margin: 1em;'><input value='$num' name='exp$name' type='radio'>" . $localizationData{"likert5_$num"} . "</input></span> ";
			}
			$text .= '\n</p>\n';
		} else {
			die "I only support 5-point likert scales for now";
		}
	}
	return $text;
}

__END__
the application works as follows:

0. handle the previous page's POST information: record results in database.

(if no experiment condition is recorded in cookie:)
1. show a welcome screen, ask for Name and other generic data 
   (this should be initialized from our database if the user has a USERID as he used the service before)
1.a show a selection of experiments available (only applies if there are multiple to choose from)
1.b assign randomly to one of the experiments/experiment conditions
   --> store experiment/experiment condition in cookie 

(if experiment condition and userID are already recorded in cookie:)
2. depending on the experiment, choose random (or next) stimulus, construct HTML
accordingly, send to user (if a stimulusCount is available, stop after maxCount is reached, thank user)

3. Experiment Definitions:
  - experiments should be in separate directories
  - stimulus HTML should be in separate files

4. Cookie format:
  - store user ID in cookie userID
  - store user name, other user information in cookies that start with "user"
  - store experiment name/condition in cookies that start with "exp"
  - we should set the httponly flag on our cookies (as we don't want the client to be able to interpret cookie information)

5. Database format:
  - store userID->user information mapping in file users.csv
  - store (userID, expID, expStimulus, rating(s)) in file results.csv
