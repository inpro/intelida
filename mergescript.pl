#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";

# merge a number of consecutive (according to a file containing cut-marks) wavesurfer files
# into one TextGrid

# Main
# Aufruf mit Pfaden als Argumente

package main;


# HIER NOCH ROBUST MACHEN MIT ARGUMENTPRUEFUNG #
# Pfade
if(@ARGV != 5)
{
	die "Nicht genug/zu viele Argumente. Geben Sie bitte die Pfade f�r folgende Daten an: \n1. PI oder e\n2. Logdateien\n3. Labdateien\n4. 
Basis-TextGrids\n5. Vom Skript zu erstellende Daten (TextGrids)\n";
}

my $fileMarker = shift;
my $logPath = shift;
my $labPath = shift;
my $tgInPath = shift;
my $tgOutPath = shift;

# fileMarker variiert je nach Korpus.
# Dateispezifikatoren, nach denen gesucht werden soll
# erkennbar an den TextGrids: <Name>_I.TextGrid oder <Name>.TextGrid
my @fileMarker = ();
if ($fileMarker eq "PI")
{
	@fileMarker =  ("_I","_E");                    # PTT, Visual_Pento
}
if ($fileMarker eq "e")
{
	@fileMarker = ("");                            # Pento_Naming, Greifarm_Woz
}

# bearbeite alle IDs
while (my $dialID = <STDIN>) 
{
	chomp $dialID; 
	chomp $dialID;

	# nimm den Praefix und oeffne das passende TextGrid
	print "DEBUG: Arbeite jetzt an $dialID.\n";
#	print "DEBUG: Jetzt kommt ein Aufruf von TextGrid::newTextGridFromFile:\n";
	my $overallTextGrid = TextGrid::newTextGridFromFile("$tgInPath/$dialID.TextGrid");

	#print $overallTextGrid;
	

	# hier ggf. if-Anweisung einfügen (grepen, nachfragen & 1. löschen und weiterlaufen lassen oder 2.abbrechen)	


	# bearbeite alle in fileMarker genannten Dateiendungen (I und E)
	foreach my $participant (@fileMarker)
	{
		# erstelle ein neues Alignment 
#		print "DEBUG: Jetzt kommt ein Aufruf von Alignment::new:\n";
		my $alignedTier = Alignment::new("${participant}-word_aligned", 0, 0, [], "lax");

               	if ($overallTextGrid->hasAlignment("${participant}-word_aligned")) {
	                warn "A tier ${participant}-word_aligned already exists in file $dialID. I will replace the old tier.\n";
			$overallTextGrid->removeAlignmentByName("${participant}-word_aligned");
		}



	# speichere Logeintraege: logs[0] entspricht dann Log fuer Unit 0
	my @logs;
	my $logFilePath = $logPath."/".$dialID.$participant.".log";
	open LOG, '<', $logFilePath or die "Cannot open file $logFilePath: $!";

	# Zaehler fuer Matches mit Log
	my $j = 0;

	foreach my $line (<LOG>)
	{
		if($line =~ m/\d+\t.*?\t[0-9.]+\t[0-9.]+\t[0-9.]+/)
		{
			$j++;
			push(@logs, $line);
			#print $line;
		}
	}
	close LOG;


# bearbeite alle Dateien bezueglich dieses Participants, um ein Alignment mit dessen Daten zu bauen	
	foreach my $labNo (0..scalar(@logs)-1)
	{
	
		# lies Lab-Datei ein und erstelle ein lokales Einzel-Alignment fuer die aktuelle Datei
		my $labFileName = $dialID.$participant."_Unit_".$labNo.".lab";
		my $alignment = Alignment::newAlignmentFromWavesurferFile($labPath."/".$labFileName);
		# l�sche Stille-Labels 
		$alignment->removeSilentLabels();
	
		# verschiebe die Zeitdaten gemae� der Gesamtwavedatei mittels @logs
		if($logs[$labNo]=~ m/\d+\t.*?\t([0-9.]+)\t[0-9.]+\t[0-9.]+/)
		{
			$alignment->timeShift($1);
		}
		else {die "Cannot shift time in $dialID, $participant, Unit $labNo: $!";}

		# lege die eben erhaltenen Daten im ParticipantAlignment ab
		$alignedTier->integrateLabelsFromAlignment($alignment, "lax");

	}#foreach labDatei

	# fuege das ParticipantAlignment dem gro�en, bereits vorliegenden TextGrid hinzu
	$overallTextGrid->addAlignmentAfter($alignedTier, $participant . '.*-Utt');

} #foreach participant

# speichere das neue, gro�e TextGrid ab
$overallTextGrid->saveToTextGridFile($tgOutPath."/".$dialID.".TextGrid");

} #while STDIN

