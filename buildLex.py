#!/usr/bin/python

import sys
import re

"""
Author: Sonntag, Jonathan
Date: 07/12/09

usage:
python buildLex.py TRANSCRIPT_1 LEXICON_1 ... TRANSCRIPT_n LEXICON_n

The order of the lexicon and transcript files have to be given like this
or strange things will happen.

New files will be created for every transcript file, that contain their
respective renamed entries. Those files will be prefixed with "NEW_".
"""

class Association:

    def __init__(self):
        self.__map__ = {}
        self.__wordnumber__ = {}

    def __makeName__(self, string, number):
        if number == 0:
            return string
        return string+"("+str(number)+")"
        
    def addEntry(self, ortho, phon):
        if ortho in self.__wordnumber__:
            # entry is already there
            for entry in self.__map__:
                if self.__map__[entry] == phon:
                    if entry.split("(")[0] == ortho:
                        return entry
                    
            # entry needs to be added
            self.__wordnumber__[ortho] = thisNumber = self.__wordnumber__[ortho] + 1
        else:
            self.__wordnumber__[ortho] = thisNumber = 0

        newOrtho = self.__makeName__(ortho, thisNumber)
        self.__map__[newOrtho] = phon

        return newOrtho
    
    def __str__(self):
        res = ""
        for entry in self.__map__:
            res += entry + "\t" + self.__map__[entry] + "\n"

        return res.strip().encode(outEncoding)

def replInTranscript(fileObj, toReplMap):
    regEx = "|".join([re.escape(a) for a in toReplMap])

    #fileStr = fileObj.read()
    outF = open("NEW_"+fileObj.name, 'w+b')

    for line in fileObj.readlines():
        line = line.decode(inEncoding)
	strToWrite = ""
        for word in line.split():
            if word in toReplMap:
		strToWrite += toReplMap[word]
                #outF.write(toReplMap[word].encode(outEncoding))
            else:
		strToWrite += word
                #outF.write(word.encode(outEncoding))
	    strToWrite += " "
            #outF.write(" ".encode(outEncoding))
	#outF.write("\n".encode(outEncoding))
        outF.write((strToWrite.rstrip()+"\n").encode(outEncoding))
    outF.close()

if __name__ == "__main__":
	inp = sys.argv[1:]
	inpIt = inp.__iter__()
	transcripts = []; lexs = [];

	while True:
	    try:
		transcripts.append(inpIt.next())
		lexs.append(inpIt.next())
	    except StopIteration:
		break


	inEncoding = "utf-8"
	outEncoding = "utf-8"

	ass = Association()

	for traName in transcripts:
	    currentTranscript = open(traName)
	    currentLex = open(lexs[transcripts.index(traName)])

	    transcriptReplaceMap = {}

	    for entry in currentLex:
		entry = entry.decode(inEncoding).strip()
		oldOrtho = entry.split("\t")[0]
		ortho = entry.split("\t")[0].split("(")[0]
		phon = entry.split("\t")[-1]

		# remove glottal stops if wanted
		if True:
		#if False:
			phonList = phon.split()

			for element in phonList:
				if element == "Q" or element == "QQ":
					del phonList[phonList.index(element)]

			phon = " ".join(phonList)

		if ortho != "":
		    newOrtho = ass.addEntry(ortho,phon)
		transcriptReplaceMap[oldOrtho] = newOrtho

	    replInTranscript(currentTranscript, transcriptReplaceMap)

	print ass
