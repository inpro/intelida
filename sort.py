#!/usr/bin/python

"""
Slow sort mechanism, but Aachen < Aachen(2) as opposed to the behaviour of sort.
This is needed to avoid sphinx problems.
"""

import sys

filename = sys.argv[1]
f = open(filename)

def order((a,b),(c,d)):
	if a < c:
		return -1
	return 1

def run(fil):
	erg = []

	for line in f:
		begin, end = line.split("\t")
		erg.append((begin,end))

	erg.sort(cmp=order)

	for begin, end in erg:
		print begin, "\t", end,

run(f)
