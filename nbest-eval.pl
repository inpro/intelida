#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;

use List::Util qw/sum/;

# read n-best results and create plot-data to show SER depending on N
# TODO: rename to something more sensible
# usage: ./nbest-eval.pl gold.transcript nbest_reco1 nbest_reco2 ...

require "Util.pm";
require "Alignment.pm";
require "TextGrid.pm";
require "NBestAlignmentSequence.pm";
require "NBestList.pm";

my $goldName = shift @ARGV or die "usage: ./nbest-eval.pl gold.transcript nbest_reco1 nbest_reco2 ...\n";
open GOLD, '<', $goldName or die "could not open gold file $goldName\n";
my %gold;
# read the gold-file (which must be in sphinx-3 transcript format) into the hash
# the keys are the file-IDs, the values the transcript
map { m/^(.*) \((.*)\)$/ or die "something did not match in gold.transcript: $_\n"; $gold{$2} = lc($1); } <GOLD>;
close GOLD;

# stores arrays for 1st, 2nd, ... -best results
my %nbests;

foreach my $file (@ARGV) {
	$file =~ m/\/?([^\/]*).nb(inc|est)_reco/ or die "something did not match: $_\n";
	print STDERR "processing file $file\n";
	my $fileID = $1; 
	die "there is no transcription in the gold file for the file $file with ID $fileID\n" unless (exists $gold{$fileID});
	open NBEST, '<', $file or die "could not open file $file\n";
	my @lines = <NBEST>;
	close NBEST;
	my $seq = NBestAlignmentSequence::newNBestSeqFromFile($file);
$seq->limitN(50);
	my $finalList = $seq->getNBestListAt(99999);
	my @nbestWords = map { lc join ' ', $_->getWords('remove') } @{$finalList->{alignments}};
	$nbests{$fileID} = \@nbestWords;
}

my $maxN = -1;
my @dist = ();
# extract the maximum N from the list of n-best-lists
# !! broken highlighting !!!
map { $maxN = Util::max($maxN, $#{$_}); $dist[$#{$_}]++; } values %nbests;
print "#distribution of n-best list sizes:\n";
for my $i (1..$#dist) {
	print "$i\t$dist[$i]\n" if defined ($dist[$i]);
}


my %SE = map { ($_, 1) } keys %nbests; # initialize SE hash to error
my %WE = map { ($_, 99999) } keys %nbests; # initialize WE to worst conceivable error
my %worstWE = map { ($_, undef) } keys %nbests; # initialize WE to worst conceivable error

my $sentencesInGold = keys %SE;
my $wordsInGold = 0;
foreach my $fileID (keys %nbests) {
	my @a = split " ", $gold{$fileID};
	$wordsInGold += $#a + 1;
}
print STDERR "sentences in gold: $sentencesInGold\n";
print STDERR "words in gold: $wordsInGold\n";
foreach my $N (0..$maxN) {
	foreach my $fileID (keys %nbests) {
		# !! broken highlighting !!!
		if ($#{$nbests{$fileID}} >= $N) { # if there is still an n-best result left
			if ($SE{$fileID} > 0) { # nothing to do if sentence error is 0
				$SE{$fileID} = 0 if ($nbests{$fileID}->[$N] eq $gold{$fileID});
			}
			my @goldWords = split ' ', $gold{$fileID};
			my @nbestWords = split ' ', $nbests{$fileID}->[$N];
			my $we = Util::levenshtein(\@goldWords, \@nbestWords);
			$worstWE{$fileID} = Util::max($worstWE{$fileID}, $we);
			$WE{$fileID} = Util::min($WE{$fileID}, $we);
		}
	} 
	my $SER = sum(values %SE) / $sentencesInGold;
	my $WER = sum(values %WE) / $wordsInGold;
	my $worstWER = sum(values %worstWE) / $wordsInGold;
	print "N = " . sprintf("%2d", $N + 1) . 
		" -> oracle SER = " . $SER . 
		",\toracle WER = " . $WER . 
		",\tanti-oracle WER = " . $worstWER . 
		"\n";
	print "no error in ";
	foreach my $key (keys %SE) {
		print "$key " if ($SE{$key} == 0);
	}
	print "\n";
}
