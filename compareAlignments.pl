#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;
use Switch;

# compare two (or more) alignments (which SHOULD contain the same labels (apart from silences) 
# and output the mean differences in label start- and end-times
# usage: ./compareAlignments.pl file1A.ortho file1B.ortho file2A.ortho file2B.ortho ...

use Statistics::Lite qw(:all);

require "Util.pm";
require "MyBrew.pm";
require "Alignment.pm";

(scalar @ARGV) % 2 == 0 or die "need an even amount of arguments.";

my (@startDiffs, @endDiffs);

while (scalar @ARGV) {
	my $fileA = shift @ARGV;
	my $fileB = shift @ARGV;
	print STDERR "comparing $fileA with $fileB\n";

	my $alA = Alignment::newAlignmentFromWavesurferFile($fileA);
	#my $seqB = NBestAlignmentSequence::newNBestSeqFromFile($fileB);
	#my $alB = $seqB->{gold};
	my $alB = Alignment::newAlignmentFromWavesurferFile($fileB);

	my @labsA = grep { !$_->isSilent() } ($alA->getLabels());
	my @labsB = grep { !$_->isSilent() } ($alB->getLabels());
	my @wordsA = map { lc $_->{text} } @labsA;
	my @wordsB = map { lc $_->{text} } @labsB;
	my $edits = MyBrew::distance(\@wordsA, \@wordsB, {-output => 'edits'});
	shift @{$edits}; # remove the leading "INITIAL"
	my ($aIndex, $bIndex) = (0, 0);

	foreach my $edit (@{$edits}) {
		switch ($edit) {
			case 'INS' { $bIndex++; }
			case 'DEL' { $aIndex++; }
			case 'SUBST' { 
				addStatsFor($labsA[$aIndex], $labsB[$bIndex]);
				$aIndex++; $bIndex++; 
			}
			case 'MATCH' { 
				addStatsFor($labsA[$aIndex], $labsB[$bIndex]);
				$aIndex++; $bIndex++; 
			}
		}
	}
}

makeStats('word beginnings', @startDiffs);
makeStats('word endings', @endDiffs);

sub addStatsFor {
	my ($labA, $labB) = @_;
	print $labA->toWavesurferLine();
	print $labB->toWavesurferLine();
	push @startDiffs, $labB->{xmin} - $labA->{xmin};
	push @endDiffs, $labB->{xmax} - $labA->{xmax};
}

sub makeStats {
	my ($text, @values) = @_;
	print "$text:\n";
	printStats(@values);
	@values = map { abs } @values;
	print "$text (absolute values):\n";
	printStats(@values);
}

sub printStats {
	my @values = @_;
	my $mean = sprintf "%.3f", mean(@values);
	my $median = sprintf "%.3f", median(@values);
	my $stddev = sprintf "%.3f", stddev(@values);
	print "mean: $mean\tmedian: $median\tstddev: $stddev\n";
}
