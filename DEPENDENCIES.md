	_____________________________________________________________
	  _                                         _           _   
	 (_)_ __  _ __  _ __ ___    _ __  _ __ ___ (_) ___  ___| |_ 
	 | | '_ \| '_ \| '__/ _ \  | '_ \| '__/ _ \| |/ _ \/ __| __|
	 | | | | | |_) | | | (_) | | |_) | | | (_) | |  __/ (__| |_ 
	 |_|_| |_| .__/|_|  \___/  | .__/|_|  \___// |\___|\___|\__|
	         |_|               |_|           |__/               
	_____________________________________________________________


to use interactivetool.pl, you need to install some dependencies:

* GTK2-Perl bindings. On Linux you can install these via the package manager.
For Ubuntu or Debian type the following into a terminal:

	`sudo apt-get install libgtk2-perl`

interactivetool.pl seems to only work on Linux, due to limitations of 
the GTK2-Perl bindings.

* JSON 
	`sudo apt-get install libjson-perl` or 
	`cpan install JSON`

* Statistics::Lite and XML::Quote modules. In a terminal type:

	`cpan install Statistics::Lite XML::Quote Term::Shell`


