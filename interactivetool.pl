#!/usr/bin/perl

use strict;
use warnings;
use Gtk2 '-init';
use Glib qw(TRUE FALSE);
use Gtk2::SimpleList;
use Statistics::Lite qw(:all);
use Data::Dumper;


use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/
require "SingularAlignmentSequence.pm";
require "Alignment.pm";
require "Histogram.pm";

##### BUGS / OPPORTUNITIES FOR IMPROVEMENT:
# - block event processing while calc() is running 
#   (i.e., change updateGtk to only update the progressbar but to skip other event processing)
# - make histogram/graph-construction configurable
# - make edit analysis graph configurable (or have adaptation to the graph content)
# - make quantiles (embedded in median-display) configurable
# - harden against mis-formatted input files
# - automatically guess number of threads for parallelization with ForkManager

my $FIXED = "fixed lagging";
my $SMOOTH = "smoothing";
my $IMPSMOOTH = "impure smoothing";

my @cmdLineArgs = @ARGV;


my $filelist; # list of files that we're dealing with

my $window = Gtk2::Window->new();
$window->set_default_size(480, 320);

my $fileSelectionPane = FileSelectionPane::createFileSelectionPane();
# $filelist has been initialized in createFileSelectionPane()
push @{$filelist->{data}}, @cmdLineArgs;

my $hbox = Gtk2::HBox->new(FALSE, 1);
$hbox->add($fileSelectionPane);
$hbox->add(ProcessingPane::createProcessingPane());
$window->add($hbox);
$window->show_all;
$window->signal_connect(delete_event => sub { Gtk2->main_quit; TRUE });
Gtk2->main;

package FileSelectionPane;
use strict;
use warnings;
use Glib qw(TRUE FALSE);

my $fc; # file chooser (module wide in order so that it will remember directories)

sub createFileSelectionPane {
	# create the file chooser dialog
	$fc = Gtk2::FileChooserDialog->new('Choose incr_reco files',
                              undef, 'open',
                              'gtk-cancel' => 'cancel',
                              'gtk-ok' => 'ok');
	# with file filters...
	my $ff = new Gtk2::FileFilter->new();
	$ff->set_name('incremental recognition results (*.inc_reco)');
	$ff->add_pattern('*.inc_reco');
	$ff->add_pattern('*.nbinc_reco');
	$fc->add_filter($ff);
	$ff = new Gtk2::FileFilter->new();
	$ff->set_name('any file');
	$ff->add_pattern('*');
	$fc->add_filter($ff);
	$fc->set_select_multiple(TRUE);

	# create the immediately visual selection pane
	$filelist = Gtk2::SimpleList->new('files' => 'text');
	my $scroll = Gtk2::ScrolledWindow->new(undef, undef);
	$scroll->set_policy('automatic', 'always');
	$scroll->add($filelist);
	my $vbox = Gtk2::VBox->new(FALSE, 1);
	$vbox->add($scroll);
	my $addButton = Gtk2::Button->new('Add ...');
	$addButton->signal_connect('clicked' => sub {
		if ('ok' eq $fc->run()) {
			push @{$filelist->{data}}, $fc->get_filenames();
		}
		$fc->hide();
	});
	$vbox->pack_start($addButton, FALSE, FALSE, 0);
	my $clearButton = Gtk2::Button->new('Clear');
	$clearButton->signal_connect('clicked' => sub {
		@{$filelist->{data}} = ();
	});
	$vbox->pack_start($clearButton, FALSE, FALSE, 0);
	return $vbox;
}

package ProcessingPane;
use strict;
use warnings;
use Glib qw(TRUE FALSE);

my $combo; # ComboBox
my $fixedLag; # SpinButton;
my $smoothness; # SpinButton
my $impureSmoothness; # SpinButton
my $impurity; # SpinButton
my $useCachedAnalysisButton; # checkbutton
my $label; # Entry

# helper to co-construct label and spinButton
sub titledSpinner {
	my ($label, $min, $max, $step) = @_;
	my $hbox = Gtk2::HBox->new(FALSE, 1);
	$hbox->add(Gtk2::Label->new($label));
	my $spinButton = Gtk2::SpinButton->new_with_range($min, $max, $step);
	$hbox->add($spinButton);
	return ($spinButton, $hbox);
}

sub createProcessingPane {
	my $optionsBox = Gtk2::VBox->new(FALSE, 3);
	$combo = Gtk2::ComboBox->new_text();
	$optionsBox->pack_start($combo, FALSE, FALSE, 0);
	my %optionPanes; # the different combo options have different option panes associated with them
	my $optionPane;

	# the first option is no processing
	$combo->append_text('none');
	$optionPanes{'none'} = Gtk2::Label->new();
	#$combo->set_active(0); # this option is the default

	# the next option is fixed lagging
	$combo->append_text($FIXED);
	($fixedLag, $optionPane) = titledSpinner('lag (in seconds)', 0.01, 1.5, 0.01);
	$optionsBox->pack_start($optionPane, FALSE, FALSE, 0);
	$optionPanes{$FIXED} = $optionPane; 

	# the next option is smoothing
	$combo->append_text($SMOOTH);
	($smoothness, $optionPane) = titledSpinner('smoothness (in frames)', 1, 100, 1);
	$optionsBox->pack_start($optionPane, FALSE, FALSE, 0);
	$optionPanes{$SMOOTH} = $optionPane;

	# the last option is impure smoothing
	$combo->append_text($IMPSMOOTH);
	$optionPane = Gtk2::VBox->new(FALSE, 1);
	my $hbox;
	($impureSmoothness, $hbox) = titledSpinner('smoothness (in frames)', 1, 100, 1);
	$optionPane->pack_start($hbox, FALSE, FALSE, 0);
	($impurity, $hbox) = titledSpinner('impurity (in frames)', 0, 99, 1);
	$optionPane->pack_start($hbox, FALSE, FALSE, 0);
	$label = Gtk2::Entry->new();
	$label->set_text("");
	$optionPane->pack_start($label, FALSE, FALSE, 0);
	$optionsBox->pack_start($optionPane, FALSE, FALSE, 0);
	$optionPanes{$IMPSMOOTH} = $optionPane;

	# automagically switch the option panes with comboBox
	$combo->signal_connect('changed' => sub {
		foreach my $optionPane (keys %optionPanes) {
			($optionPane eq $combo->get_active_text())
				? $optionPanes{$optionPane}->show()
				: $optionPanes{$optionPane}->hide();
		}
	});
	$combo->set_active(0); # set 'none' as default
	my $optionFrame = Gtk2::Frame->new('Processing options');
	$optionFrame->add($optionsBox);
	my $outerPane = Gtk2::VBox->new(FALSE, 1);
	$outerPane->pack_start($optionFrame, FALSE, FALSE, 0);
	$outerPane->pack_start(Gtk2::HBox->new(), TRUE, TRUE, 0);
	$useCachedAnalysisButton = Gtk2::CheckButton->new("Use cached analyses");
	$outerPane->pack_start($useCachedAnalysisButton, FALSE, FALSE, 0);
	my $analysisButton = Gtk2::Button->new('Analyze ...');
	$analysisButton->signal_connect('clicked' => sub { doAnalysis(); });
	$outerPane->pack_start($analysisButton, FALSE, FALSE, 0);
	return $outerPane;
}

sub doAnalysis {
	my $processor = ($combo->get_active_text() or "none");
	my $windowTitle = $label->get_text() 
			. " ($processor";
	my $procFunc;
	if ($processor eq $FIXED) {
		$procFunc = sub { $_[0]->fixedLagSequence($fixedLag->get_value()) };
		$windowTitle .= " with lag " . $fixedLag->get_value();
	} elsif ($processor eq $SMOOTH) {
		my $smoothnessValue = $smoothness->get_value_as_int();
		$procFunc = sub { $_[0]->smoothSequence($smoothnessValue) };
		$windowTitle .= " with smoothness " . $smoothnessValue;
	} elsif ($processor eq $IMPSMOOTH) {
		my $smoothnessValue = $impureSmoothness->get_value_as_int();
		my $impurityValue = $impurity->get_value_as_int();
		$procFunc = sub { $_[0]->impureSmoothSequence($smoothnessValue,
							   $impurityValue) };
		$windowTitle .= " with smoothness " . $smoothnessValue;
		$windowTitle .= " and impurity " . $impurityValue;
	} else { # "none" was selected
		$procFunc = sub { $_ }; # none
	}
	my @files = map { @{$_}[0] } @{$filelist->{data}}; # get the first column of the list
	my $useAnalysisCache = $useCachedAnalysisButton->get_active();
	$windowTitle .= ")";
	AnalysisWindow::createAnalysisWindow(\@files, $procFunc, $windowTitle, $useAnalysisCache)->draw();
}

package AnalysisWindow;
use strict;
use warnings;
use Glib qw(TRUE FALSE);
use Statistics::Lite qw(:all);
use Digest::SHA qw(sha512_hex); # for usage with cached data
use Storable; # loading cached data
use Data::Dumper;

# fill in data in the beginning (immediately triggers call to &calc() via &getData())
sub draw {
	my ($self) = @_;
	### draw EO panel ###
	my %stats = %{$self->getData('eoHash')};
	while (my ($stat, $entry) = each %{$self->{eoStats}}) {
		$entry->set_text($stats{$stat});
	}
	my @values = @{$self->getData('hypAges')};
	%stats = statshash(@values);
	my $histogram = Histogram::new([@values]);
	$histogram->createBinBoundaries('equidistant', 0.01, $stats{min}, int($stats{range} / 0.01));
	$histogram->{usePercent} = 1;
	$histogram->{accumulateCounts} = 1;
	my $tempfile = $histogram->getTempImage('png', 'transparent');
	$self->{hypAgesPlot}->set_from_file($tempfile);
	$self->{hypAgesPlotText} = $histogram->toGnuplotString();
	### draw TIMING panel ###
	$self->redrawTiming(); 
	### draw non-incremental panel ###
	$self->redrawNonIncrementalTab();
}

sub redrawTiming {
	my $self = shift;
	my $mode = $self->{WERmode};
	if ($mode eq 'all') { $mode = '' } else { $mode = "_$mode" }
	my %delays = ($self->{FOFD} eq 'first_occurrence')
		? %{$self->getData("foDelays$mode")}
		: %{$self->getData("fdDelays$mode")};
	my @values = sort {$a <=> $b} @{$delays{$self->{timingStrategy}}};
	my %stats = statshash(@values);
	my $quantverylow = $values[($#values * .05)];
	my $quantlow = $values[($#values * .25)];
	my $quanthigh = $values[($#values * .75)];
	my $quantveryhigh = $values[($#values * .95)];
	$stats{median} .= " (5\% quantile: $quantverylow, first quartile: $quantlow, third quartile: $quanthigh, 95\% quantile: $quantveryhigh)";
	while (my ($stat, $entry) = each %{$self->{timingStats}}) {
		$entry->set_text($stats{$stat});
	}
	my $histogram = Histogram::new([@values]);
	my $histMin = $self->{histSetts}->{'histogram min'}->get_text();
	my $histMax = $self->{histSetts}->{'histogram max'}->get_text();
	my $histStep = $self->{histSetts}->{'histogram step'}->get_text();
	my $histCount = int (($histMax - $histMin) / $histStep);
	$histogram->createBinBoundaries('equidistant', $histStep, $histMin, $histCount);
	$histogram->{usePercent} = 1;
	my $tempfile = $histogram->getTempImage('png', 'transparent');
	$self->{timingPlot}->set_from_file($tempfile);
	$self->{timingPlotText} = $histogram->toGnuplotString();
}

sub redrawNonIncrementalTab {
	my $self = shift @_;
	my %stats = %{$self->{niHash}};
	while (my ($stat, $entry) = each %{$self->{niStats}}) {
		$entry->set_text(exists($stats{$stat}) ? $stats{$stat} : "n/a");
	}
}

# called when a radiogroup on the Timing Analysis tab is changed
# calls &redrawTiming() which may trigger re-computation in &calc() 
sub updateTimingOptions {
	my $button = shift @_;
	my $self = $_[0][0];
	my $what = $_[0][1];
	# if WERmode is first changed, we need to ask for a goldtranscript file
#	$self->assertTranscript() if ($what eq 'WERmode');
	$self->{$what} = $button->get_label();
	$self->{$what} =~ s/ /_/g;
	$self->redrawTiming();
}

# called when subst/nosubst choice is changed in Edit Analysis tab
# directly changes the (already pre-computed) data display
sub updateEOOption {
	my $button = shift @_;
	my $self = $_[0][0];
	my $useSubstitution = $button->get_active();
	my %stats = %{$self->getData('eoHash')};
	my %eoStats = %{$self->{eoStats}};
	while (my ($stat, $entry) = each %eoStats) {
		$entry->set_text($stats{$stat});
	}
	if (!$useSubstitution) {
		my $ADDwoSubst = $stats{addIUs} + $stats{substIUs};
		$eoStats{addIUs}->set_text($ADDwoSubst);
		my $REVOKEwoSubst = $stats{revokeIUs} + $stats{substIUs};
		$eoStats{revokeIUs}->set_text($REVOKEwoSubst);
		$eoStats{substIUs}->set_text("n/a");
		my $EOwoSubst = ($stats{changes} - $stats{words}) / $stats{changes}; ## traditional form of EO calculation
		$eoStats{EO}->set_text($EOwoSubst);
	}
}

# make sure there's a 
sub updateNonIncrementalTab {
	my $button = shift @_;
	my $self = $_[0][0];
#	if (!(exists $self->{'goldtranscript'})) {
#		$self->assertTranscript();
#		$self->calc();
#	}
	$self->redrawNonIncrementalTab();
}

use Parallel::ForkManager;

sub calc {
	my $self = shift;
	# now, this is really getting ugly with so many lists...
	my %delays_full = ( FO_all => { from_start => [], to_end => [], word_rel => [] },
			    FD_all => { from_start => [], to_end => [], word_rel => [] },
			    FO_match => { from_start => [], to_end => [], word_rel => [] },
			    FD_match => { from_start => [], to_end => [], word_rel => [] },
			    FO_matchsubst => { from_start => [], to_end => [], word_rel => [] },
			    FD_matchsubst => { from_start => [], to_end => [], word_rel => [] },);
	my %eoHash = ();
	# all hypothesis ages (to plot stability graph)
	my @hypAges;
	# in order to compute WER later on
	my @totalAlignmentEdits;
	my $goldWordCount = 0;
	# for the progress bar:
	my $step;
	my $totalSteps = scalar @{$self->{filelist}};
	$self->{progressBar}->show();
	# fork manager for parallelization:
	my $forkmanager = Parallel::ForkManager->new(6); # number of simultaneous children
	$forkmanager->run_on_finish( sub {
		my ($pid, $exit_code, $ident, $signal, $core_dump, $file_data) = @_;
		# update our nice little progress bar
		$self->updateProgressbar(++$step, $totalSteps);
		if (defined($file_data)) {  # children are not forced to send anything ...
			while (my ($stat, $value) = each %{$file_data->{eoHash}}) {
				$eoHash{$stat} += $value;
			}			
			push @hypAges, @{$file_data->{hypAges}};
			# write to aggregation datastructure
			foreach my $type ('all', 'match', 'matchsubst') {
				foreach my $list ('from_start', 'to_end', 'word_rel') {
					push @{$delays_full{"FO_$type"}{$list}}, @{$file_data->{"FO_$type"}{$list}};
					push @{$delays_full{"FD_$type"}{$list}}, @{$file_data->{"FD_$type"}{$list}};
				}
			}
			push @totalAlignmentEdits, @{$file_data->{alignmentEdits}};
			$goldWordCount += $file_data->{goldWordCount};
		} else {
			die; # ... but they better do in our case
		}
	});
#	$self->assertTranscript();
	# run in parallel (it's REALLY ugly, that ForkManager uses a goto statement...)
	FILELOOP:
	foreach my $filename (@{$self->{filelist}}) {
		$forkmanager->start() and next FILELOOP;
#		# update our nice little progress bar
#		$self->updateProgressbar(++$step, $totalSteps);
		my %file_data; #eoHash => {}, hypAges => []};
		# open file
		my $alignmentSeq = SingularAlignmentSequence::newSeqFromFile($filename);
		$alignmentSeq->makeContinuous();
		# apply the processor
		&{$self->{procFunc}}($alignmentSeq);
		# evaluate
		# EO:
		%{$file_data{eoHash}} = $alignmentSeq->getEditOverheadHash();
		@{$file_data{hypAges}} = $alignmentSeq->getHypothesisAges();
		# basic timing:
		%{$file_data{FO_all}} = $alignmentSeq->getFODelays();
		%{$file_data{FD_all}} = $alignmentSeq->getFDDelays();
		if (!exists $self->{skiptranscript}) { # work to be done if transcript is available:
			my $transcript;
			# acquire transcript:
			if ($alignmentSeq->getGold() != $alignmentSeq->getLastAlignment()) {
				# this file has it's own gold alignment attached 
				$transcript = join " ", $alignmentSeq->getGold()->getWords();
			} else {
				$transcript = $self->getTranscript($filename);
#say $transcript;
			}
			for my $mode ('match', 'matchsubst') {
				# compute WER-dependent timing metrics:
				%{$file_data{"FO_$mode"}} = $alignmentSeq->_grepFOFDWER(\%{$file_data{FO_all}}, $mode, $transcript);
				%{$file_data{"FD_$mode"}} = $alignmentSeq->_grepFOFDWER(\%{$file_data{FD_all}}, $mode, $transcript);
			}
			# WER information:
			@{$file_data{alignmentEdits}} = @{$alignmentSeq->getWEList($transcript)};
			$file_data{goldWordCount} = scalar split " ", $transcript;
		}
		$forkmanager->finish(0, \%file_data);
	}
	$forkmanager->wait_all_children();
	$self->{progressBar}->hide();
	my $edits = $eoHash{addIUs} + $eoHash{revokeIUs} + $eoHash{substIUs};
	$eoHash{EO} = ($edits != 0) ? ($edits - $eoHash{words}) / $edits : 0;
	$self->{eoHash} = \%eoHash;
	$self->{foDelays} = \%{$delays_full{'FO_all'}};
	$self->{fdDelays} = \%{$delays_full{'FD_all'}};
	$self->{hypAges} = \@hypAges;
	$self->{niHash} = { files => $totalSteps, 'hyp words' => $eoHash{words}};
	if (!exists $self->{skiptranscript}) { # if goldtranscript doesn't exist, then we don't want to put anything here
		$self->{foDelays_match} = \%{$delays_full{'FO_match'}};
		$self->{foDelays_matchsubst} = \%{$delays_full{'FO_matchsubst'}};
		$self->{fdDelays_match} = \%{$delays_full{'FD_match'}};
		$self->{fdDelays_matchsubst} = \%{$delays_full{'FD_matchsubst'}};
		#@totalAlignmentEdits = map {@$_} @totalAlignmentEdits; # flatten the list of lists
		my $WE = scalar grep { m/INS|DEL|SUBST/ } @totalAlignmentEdits;
		$self->{niHash}->{WER} = ($goldWordCount != 0) ? $WE / $goldWordCount : 0;
		$self->{niHash}->{"gold words"} = $goldWordCount;
	}
#	unless (-e $self->{storeName}) { 
	# it's probably wise to unconditionally save results, even if old results already exist
		my %data = ( eoHash => \%eoHash, 
			     foDelays => \%{$delays_full{'FO_all'}},
			     fdDelays => \%{$delays_full{'FD_all'}},
			     hypAges => \@hypAges );
		open PLS, "|gzip -c > $self->{storeName}" or die "oups: $!, $^E";
		Storable::store_fd(\%data, *PLS);
		close PLS;
#	}
}

##### guarded access to contained data (general data and transcript) #####

sub getData {
	my ($self, $data) = @_;
	$self->calc() unless (exists $self->{$data});
	return ($self->{$data});
}

# return the transcript that is stored for a file (asking for transcript file if necessary)
sub getTranscript {
	my ($self, $filename) = @_;
	$self->assertTranscript();
	$filename =~ m/([^\/]*)\.inc_reco/;
	my $file = $1;
	my $transcript = $self->{'goldtranscript'}->{$file};
	print STDERR "no transcript found for $filename under ID $file\n" unless ($transcript);
	return $transcript;
}

# ask for transcript file (unless it has been read)
sub assertTranscript {
	my $self = shift @_;
	if (!(exists $self->{'goldtranscript'})) {
		my $fc = Gtk2::FileChooserDialog->new('Choose gold transcript file',
		                      undef, 'open',
		                      'gtk-cancel' => 'cancel',
		                      'gtk-ok' => 'ok');
		my $returnCode = $fc->run();
#		do { $returnCode = $fc->run(); } until ($returnCode eq 'ok');
		$fc->hide();
		my $file = $fc->get_filename();
		my %gold = Util::readSphinx3Transcript($file);
		$self->{'goldtranscript'} = \%gold;
	}
}

# update the progress bar to a given "X of Y"
sub updateProgressbar {
	my ($self, $curr, $max) = @_;
	my $progress = $curr / $max;
	$self->{progressBar}->set_text("processing file $curr of $max"); 
	$self->{progressBar}->set_fraction($progress);
	updateGtk();
}

# update GTK main loop, to trigger UI changes; 
# used for updating the progress bar
sub updateGtk {
	while (Gtk2->events_pending) {
		Gtk2->main_iteration;
	}
}

##### helpers for tables/radiogroups  #####

# get a new HBox which contains radio buttons in a group
# add the titles that you want as parameters
# only works with the Timing tab
sub newTimingRadioGroup {
	my ($self, $group, @items) = @_;
	my $hbox = Gtk2::HBox->new(FALSE, 1);
	my $prev = undef;
	map { 
		my $radio = Gtk2::RadioButton->new($prev, $_);
		$radio->signal_connect(
			'toggled' => \&AnalysisWindow::updateTimingOptions, 
			[$self, $group]);
		$hbox->pack_start($radio, FALSE, FALSE, 0);
		$prev = $radio;
	} (@items);
	return ($prev, $hbox);
}

# create a number of (named, uneditable) text fields that display the 
# information which is the correspondingly named fields 
sub createStatsTable {
	my ($self, $type, @stats) = @_;
	my $table = Gtk2::Table->new(scalar @stats, 2, FALSE);
	$self->{$type} = {};
	my $row = 0;
	foreach my $stat (@stats) {
		$table->attach_defaults(Gtk2::Label->new($stat), 0, 1, $row, $row + 1);
		my $entry = Gtk2::Entry->new();
		$entry->set_editable(FALSE);
		$table->attach_defaults($entry, 1, 2, $row, $row + 1);
		$self->{$type}->{$stat} = $entry;
		$row++;
	}
	return $table;
}

sub createSettingsTable {
	my ($self, $type, @settings) = @_;
	my $table = Gtk2::Table->new(scalar @settings, 2, FALSE);
	$self->{$type} = {};
	my $row = 0;
	foreach my $setting (@settings) {
		$table->attach_defaults(Gtk2::Label->new($setting), 0, 1, $row, $row + 1);
		my $entry = Gtk2::Entry->new();
		$table->attach_defaults($entry, 1, 2, $row, $row + 1);
		$self->{$type}->{$setting} = $entry;
		$row++;
	}
	return $table;
}

# generate a plotted figure including access to the plot source
sub plotWithPopup {
	my ($self, $plotName) = @_;
	my $plot = Gtk2::Image->new();
	$self->{$plotName} = $plot;
	my $eventbox = Gtk2::EventBox->new();
	$eventbox->add($plot);
	my $menu = Gtk2::Menu->new();
	my $copyitem = Gtk2::MenuItem->new("copy gnuplot code to clipboard");
	$copyitem->signal_connect(activate => sub {
		my $clipboard = Gtk2::Clipboard->get(Gtk2::Gdk->SELECTION_PRIMARY);
		$clipboard->set_text($self->{$plotName . "Text"});
	});
	$copyitem->show();
	$menu->append($copyitem);
	$eventbox->signal_connect('button-press-event' => sub {
		my ($widget, $event) = @_;
		return FALSE unless ($event->button == 3);
		$menu->popup(undef, undef, undef, undef, $event->button, $event->time);
	});
	$self->{$plotName . "Text"} = "";
	return $eventbox;
}

sub createAnalysisWindow {
	my ($filelist, $procFunc, $windowTitle, $useAnalysisCache) = @_;
	my $storeName = sha512_hex($windowTitle . join ", ", @{$filelist}) . ".pls.gz";
	my $self = bless { 
		filelist => $filelist, 
		procFunc => $procFunc, 
		FOFD => 'first_occurrence', 
		WERmode => 'all',
		timingStrategy => 'from_start',
		storeName => $storeName,
	};
	if ($useAnalysisCache) { # load previously cached analysis...
		if (-e $storeName) {
			open PLS, "gunzip -c $storeName | " or die "oups: $!";
			my $data = Storable::retrieve_fd(*PLS);
			close PLS;
			$self->{eoHash} = $data->{eoHash};
			$self->{foDelays} = $data->{foDelays};
			$self->{fdDelays} = $data->{fdDelays};
			$self->{hypAges} = $data->{hypAges};
		}
	}
	### definition of the timing tab ###
	my $timingBox = Gtk2::VBox->new(FALSE, 1);
	my $settingsBox = Gtk2::HBox->new(FALSE, 1);
	# add FO/FD selection settings:
	my $FOFDBox = Gtk2::VBox->new(FALSE, 1);
	my ($FOFD, $hbox) = $self->newTimingRadioGroup('FOFD', 'first occurrence', 'final decision');
	$FOFDBox->pack_start($hbox, FALSE, FALSE, 0);
	(my $FOFDPOS, $hbox) = $self->newTimingRadioGroup('timingStrategy', 'from start', 'to end', 'word rel');
	$FOFDBox->pack_start($hbox, FALSE, FALSE, 0);
	(my $WERmode, $hbox) = $self->newTimingRadioGroup('WERmode', 'all', 'match', 'matchsubst');
	$FOFDBox->pack_start($hbox, FALSE, FALSE, 0);
	$settingsBox->pack_start($FOFDBox, FALSE, FALSE, 0);
	# add histogram settings:
	my $histogramSettingsBox = Gtk2::VBox->new(FALSE, 1);
	$histogramSettingsBox->pack_start(
		$self->createSettingsTable('histSetts', 
					   'histogram min', 'histogram max', 'histogram step'),
		FALSE, FALSE, 0);
	$self->{histSetts}->{'histogram min'}->set_text("-1");
	$self->{histSetts}->{'histogram max'}->set_text("2.5");
	$self->{histSetts}->{'histogram step'}->set_text("0.1");
	$settingsBox->pack_start($histogramSettingsBox, FALSE, FALSE, 0);
	# add all settings:
	$timingBox->pack_start($settingsBox, FALSE, FALSE, 0);
	# add results to timing box:
	$timingBox->pack_start(
		$self->createStatsTable('timingStats', 
					'mean', 'stddev', 'median', 'count', 'min', 'max'), 
		FALSE, FALSE, 0);
	my $timingPlot = $self->plotWithPopup('timingPlot');
	$timingBox->pack_start($timingPlot, FALSE, FALSE, 0);
	### definition of the EO tab ###
	my $eoBox = Gtk2::VBox->new(FALSE, 1);
	my $EOwSubstButton = Gtk2::CheckButton->new("use substitution edits");
	$EOwSubstButton->set_active(TRUE);
	$EOwSubstButton->signal_connect(
		'toggled' => \&AnalysisWindow::updateEOOption,
		[$self, $EOwSubstButton->get_active()]);
	$eoBox->add($EOwSubstButton);
	$eoBox->add(Gtk2::VBox->new()); # add flexible padding
	$eoBox->pack_start(
		$self->createStatsTable('eoStats',
					'addIUs', 'substIUs', 'revokeIUs', 'EO', 'words'),
		FALSE, FALSE, 0);
	my $hypAgesPlot = $self->plotWithPopup('hypAgesPlot');
	$eoBox->pack_start($hypAgesPlot, FALSE, FALSE, 0);
	### definition of non-incremental metrics tab ###
	my $nonIncrBox = Gtk2::VBox->new(FALSE, 1);
	my $computeNonIncrButton = Gtk2::Button->new('Select Transcript...');
	$computeNonIncrButton->signal_connect(
		'pressed' => \&AnalysisWindow::updateNonIncrementalTab,
		[$self]);
	$nonIncrBox->add($computeNonIncrButton);
	$nonIncrBox->add(Gtk2::VBox->new()); # add flexible padding
	$nonIncrBox->pack_start(
		$self->createStatsTable('niStats',
					'WER', 'hyp words', 'gold words', 'files'),
		FALSE, FALSE, 0);
	### combine the three tabs and display
	my $notebook = Gtk2::Notebook->new();
	$notebook->append_page($timingBox, 'Timing Analysis');
	$notebook->append_page($eoBox, 'Edit Analysis');
	$notebook->append_page($nonIncrBox, 'WER Analysis');
	my $layoutTable = Gtk2::Table->new(2, 3, FALSE);
	$layoutTable->attach_defaults($notebook, 0, 2, 0, 1);
	#$layoutTable->attach_defaults($timingPlot, 0, 2, 1, 2);
	my $progressBar = Gtk2::ProgressBar->new();
	$progressBar->set_orientation('left-to-right');
	$self->{progressBar} = $progressBar;
	$layoutTable->attach_defaults($progressBar, 0, 2, 2, 3);
	my $window = Gtk2::Window->new();
	$window->add($layoutTable);
	$window->set_title("Analysis window -- Processing: $windowTitle");
	$window->set_default_size(480, 320);
	$window->show_all;
	return $self;
}

__END__

