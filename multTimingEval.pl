#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;
use Statistics::Lite qw(:all);
require "NBestAlignmentSequence.pm";
require "NBestList.pm";
require "Alignment.pm";
require "Label.pm";
require "Histogram.pm";

# measures calculated:
#
# when a word was first rightly recognized in any hypothesis (delay_{fr})
# when a word was last not correctly recognized in any hypothesis (delay_{lw})
# these measures will still be calculated with regard to the "best" final hypothesis 
# (with "best" meaning either the best ranked or with min WER compared to gold)
# TODO:
# additionally computes the "correction time", which is the difference
# between delay_fr and delay_lw
# and shows the distribution of correction times

#
# silence markers are skipped in evaluation! (they're not words after all)

# usage:
# ./multTimingEval.pl gold.transcript data/woz/*.nbinc_reco

########## main program #############

package main;

my %gold = Util::readSphinx3Transcript(shift @ARGV);
foreach my $key (keys %gold) {
	$gold{$key} = [split " ", $gold{$key}];
}
my $limit = shift @ARGV;
my %delays_FO_total = ( from_start => [], to_end => [], word_rel => [] );
foreach my $file (@ARGV) {
	$file =~ m/.*?\/?(.*).nbinc_reco/ or die "$file is weird";
	my $fileID = $1;
	my $seq = NBestAlignmentSequence::newNBestSeqFromFile($file);
	$seq->limitN($limit - 1);
	exists $gold{$fileID} or die "no transcript for $fileID\n";
	$seq->selectBestFinalAsGold(@{$gold{$fileID}});
	my %delays_FO = $seq->getFODelays();
	foreach my $key (keys %delays_FO_total) {
		push @{$delays_FO_total{$key}}, @{$delays_FO{$key}};
	}
}
#foreach my $key (keys %delays_FO_total) {
#	print "$key: " . join(" ", @{$delays_FO_total{$key}}) . "\n";
#}
my @values = sort { $a <=> $b } @{$delays_FO_total{from_start}};
my $quantlow = sprintf "%.3f", $values[($#values * .05)];
my $quanthigh = sprintf "%.3f", $values[($#values * .95)];
my $quartlow = sprintf "%.3f", $values[($#values * .25)];
my $quarthigh = sprintf "%.3f", $values[($#values * .75)];
my %stats = statshash(@values);
#print "limit: $limit\n";
#foreach my $key ('mean', 'stddev', 'median', 'count', 'min', 'max') {
#	print "$key: $stats{$key}\n";
#}
#print "#n-bestsize\t5%\t25%\tmedian\t75%\t95%\tmean\tstddev\tcount\n";
print "$limit\t$quantlow\t$quartlow\t$stats{median}\t$quarthigh\t$quanthigh\t$stats{mean}\t$stats{stddev}\t$stats{count}\n";
#print "5%: $quantlow; 25%: $quartlow; 75%: $quarthigh; 95%: $quanthigh\n";
#my $histogram = Histogram::new([@values]);
#$histogram->createBinBoundaries('equidistant', 0.100, -1, 30);
#$histogram->{usePercent} = 1;
#print $histogram->toGnuplotString();

__END__
fully correct utterances:
{vp24_9,vp25_22,vp1_26,vp25_10,vp25_16,vp26_19,vp25_8,vp5_23,vp25_11,vp25_21,vp1_6,vp5_6,vp25_4,vp26_16}.nbinc_reco
oracle-correct utterances:
{vp24_9,vp25_22,vp25_20,vp26_10,vp1_26,vp25_10,vp5_27,vp25_16,vp25_6,vp24_14,vp25_5,vp26_15,vp26_19,vp25_8,vp3_26,vp5_23,vp25_18,vp25_11,vp25_21,vp1_6,vp5_6,vp22_18,vp25_4,vp1_18,vp26_2,vp26_16,vp22_23,vp26_20,vp26_9}.nbinc_reco

