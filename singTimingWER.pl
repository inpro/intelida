#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;
use Statistics::Lite qw(:all);
require "SingularAlignmentSequence.pm";
require "Alignment.pm";
require "Label.pm";
require "Util.pm";
require "MyBrew.pm";
use Switch;

# goal (for now):
# - extract timing metrics for words correctly recognized (compared to a gold transcript) from lastAlignment 
# goal (potentially): use gold timing from gold standard (we don't really have that)
# plan: 
# - load gold transcript and load file
# - find list of edits that gives minimum edit distance; 
# - calculate getFO() (keeping word identity in the individual entries)
# - follow this list, keep only the items from getFO() that have a MATCH edit.

my $goldName = shift @ARGV or die "usage: ./singTimingWEReval.pl gold.transcript inc_reco1 inc_reco2 ...\n";
open GOLD, '<', $goldName or die "could not open gold file $goldName\n";
my %gold;
# read the gold-file (which must be in sphinx-3 transcript format) into the hash
# the keys are the file-IDs, the values the transcript
map { m/^(.*) \((.*)\)$/ or die "something did not match in gold.transcript: $_\n"; $gold{$2} = lc($1); } <GOLD>;
close GOLD;

foreach my $file (@ARGV) {
	$file =~ m/\/([^\/]*)\.inc_reco/ or die "something did not match: $file\n";
	my $fileID = $1; 
	die "there is no transcription in the gold file for the file $file with ID $fileID\n" unless (exists $gold{$fileID});
	my $seq = SingularAlignmentSequence::newSeqFromFile($file);
	$seq->getFODelaysWER('all', $gold{$fileID});
}

