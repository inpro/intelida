#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

require "Util.pm";
require "TextGrid.pm";
require "Wave.pm";

use utf8;
binmode STDIN, ':utf8';
binmode STDOUT, ':utf8';
use open qw(:std :utf8);

my %turns; 

my $filename = $ARGV[0] or die "I need a SemAlign-Text file as first argument";
$filename =~ m/(D[12])\_\d+.txt/ or die "that's a weird filename: $filename";
my $day = $1;

my $tgname = $filename;
$tgname =~ s/.txt$/.TextGrid/;
my $tg = TextGrid::newTextGridFromFile($tgname);
my $IG = $tg->getAlignmentByName('IG');

# read the file information into the turns hash
while (my $line = <>) {
	chomp $line;
	chomp $line;
	next if ($line =~ m/^----/);
	next if ($line =~ m/^#/);
	next if ($line =~ m/^agent_connect to Facilitator/);
	my ($file, $turn, $key, $val) = $line =~ m/(\d+)\.(\d+)\:(.+?)\:\s*(.*)/ or die "could not parse: $line\n";
	my $id = "$file.$turn";
	if (!exists $turns{$id}) {
		$turns{$id} = {};
		$turns{$id}->{file} = $file;
		$turns{$id}->{turn} = $turn;
	}
	if ($key eq 'utt') {
		
	}
	$turns{$id}->{$key} = $val;
}

# process the turns
while (my ($turnID, $turnInfo) = each %turns) {
	# save gold text files
	my $goldfilename = "gold/$day" . "_$turnID" . ".goldtext";
	my $goldText = $IG->getLabelAt(($turnInfo->{xmin} + $turnInfo->{xmax}) / 2)->{text};

	open GOLDTEXT, '>', $goldfilename;
	print GOLDTEXT Util::filterTranscript($goldText);
	print GOLDTEXT "\n";
	close GOLDTEXT;
	# cut audio
	my $audiooutfilename = "wav/$day" . "_$turnID" . ".wav";
	my $audioinfilename = "wav/$day" . "_$turnInfo->{file}" . ".wav";
#	my $wave = Wave::newFromFile($audioinfilename);
#	$wave->saveSpan($turnInfo->{xmin}, $turnInfo->{xmax}, $audiooutfilename);
}
