#!/usr/bin/perl

# Angelika Adam 28.10.2008
# word-extract.pl
# Diese Skript liest *.TextGrid-Files aus einem beim Skriptaufruf �bergebenen
# bzw. wenn nix �bergeben aus dem aktuellen Verzeichnis ein.
# Daraus wird eine Ausgabe-Datei result.txt erstellt.
# Diese enth�lt die W�rter der konkreten �u�erungen in alphabetischer Reihenfolge.
# Interpunktionszeichen (,;.:-!?()) werden gefiltert,
# ebenso wie leere Textfelder(text= "") im TextGrid.

use FindBin;
use lib "$FindBin::Bin/lib";
use Switch;

use strict;
use warnings;
require "TextGrid.pm";


#Global Hash
my %words;

#Globaler Name des Output Files
my $WriteTo = "results.txt";

#Pfad zu den TextGrid Dateien
my $PathToExermine;
$PathToExermine = $ARGV[1];

#Korpus-Marker - beim Erzeugen der Lexika verwendet
my $ID=$ARGV[0];

if($PathToExermine ne ''){
      print "Parameter �bernommen: " . $PathToExermine . "\n";
}else{
      print "Keinen Pfad-Parameter erhalten.\n";
      $PathToExermine = '';
}
GetAllFiles($PathToExermine);

WriteToFile();

sub GetAllFiles{
      my @FileList;

	my $path = $_[0];

      if($path eq ''){
            print "Verzeichnis: aktuelles Verzeichnis\n";
		$path = "./";
      }else
      {
            print "Verzeichnis: " . $_[0]  . "\n";
      }
		opendir(DIR,$path);
      while(my $datei = readdir(DIR))
      {
            if ($datei =~ /.\.TextGrid$/)
            {
                  ProcessFile("$path/$datei");
            }
      }
      closedir(DIR);
      
      #foreach my $key (sort keys %words)
      # {
      #         print $key. "=>". $words{$key}."\n";
      # }
      
      foreach my $key (sort { $words{$b} <=> $words{$a} } keys %words)
      {
              print $key. " => ". $words{$key}."\n";
      }
}



sub ProcessFile{

      my $gridfile = $_[0];
      
      #Array zum Zwischenspeichern der �u�erungen
      my @utt_list;
      
      # Variable zur Mustererkennung
      my $test = '';		
      
      print STDERR "Oeffne Datei: " . $gridfile . "\n";
      # oeffnen der Textgrid-Datei
#      open(FILE, $gridfile) or die "could not open file $gridfile\n";

      # Fallunterscheidung beim Erzeugen der Lexika;
      # in den Korpora werden unterschiedliche Namen für die Tiers verwendet, aus denen die Wörter extrahiert werden	
      my @ID=();
      switch ($ID) {
	case "single1" {@ID=("userTranscript")}		# Greifarm_WOZ, WOZ
	case "single2" {@ID=("utterances")}		# Pento_Naming

	# Sprecherrollen bei "dual channel corpora" vertauscht
	case "dual1"   {@ID=("P-utts", "E-utts")}		# P2T
	case "dual2"   {@ID=("Player-Utt", "Instr-Utt")}	# Visual_Pento
	else 	       {print "ID für Tiernamen unbekannt"}
      }

		my $tg = TextGrid::newTextGridFromFile($gridfile);
		for my $tierName (@ID) {
			my $tier = $tg->getAlignmentByName($tierName);
			print $tier;
			my @words = $tier->getWords('remove');

			@words = map { split / / } @words;
			foreach my $word (@words) {
				$word =~ s/[\{\}\(\)\.\+\,\:\!\?\-\[\]\\\"\/\`]//g; # remove punctuation
				$words{$word}++;
				# print "$word, \n";
			}
		}
#      while ($test=<FILE>) 			  # zeilenweises Durchgehen der Datei
 #     {
  #    
#        chomp $test;                            # Entfernen des Zeilenumbruchs
#        $test =~ s/\;|\?|\.|\,|:|\!|\(|\)//g;        #rausfiltern der Interpunktionszeichen
#        if ($test=~/text = "(.*)"/)		  # Suche nach Aeusserungen, ohne Leerzeilen, Instructor- und Player-Eintrag
 #       {
#      
#              if ($1 ne '')
#              {
#                      
#                      push (@utt_list,$1);
#      
#              }
#        }
#      }
      
      # Entfernen der Player/ Instructor-Eintr�ge und MetaTags in der Liste:
       
#      @utt_list = grep !/(Player|Instructor)|(\.*<.*\>.*)/, @utt_list;
      
       
#      close FILE;	
      
#      my @word_list;
      
#              foreach my $element (@utt_list)
#              {
                      
#                      push (@word_list,(split (" ", $element)));
      
#              }
      
#      foreach my $word (@word_list)
#      {
#              $words{$word}++;
#      }
      
}

sub WriteToFile{
      open(SCHREIBEN,"> ".$WriteTo)
            or die "Fehler beim �Offnen der Ausgabedatei!\n";
      
      #foreach my $key (sort keys %words)
      foreach my $key (sort { lc($a) cmp lc($b) } keys %words)
       {
               #print SCHREIBEN $key. "=>". $words{$key}."\n";
               
               #nur Ausgabe der einzelnen W�rter:
               print SCHREIBEN $key."\n";
       }
       
       close(SCHREIBEN);
}
