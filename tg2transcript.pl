#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;

require "TextGrid.pm";
require "Alignment.pm";

my ($filename, $tiername) = @ARGV;

my $tg = TextGrid::newTextGridFromFile($filename);
my $al = $tg->getAlignmentByName($tiername);
print join " ", $al->getWords();
