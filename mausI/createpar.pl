#!/usr/bin/perl

use strict;
use warnings;

# createpar.pl creates a par file from orthography and pronunciation
# USAGE: perl createpar.pl <basename>

# PSP08, A. Iwanow 

# Intro of a parfile
print "LHD: Partitur 1.2\n";
print "REP: Munich, Germany\n";
print "SNB: 2\n";
print "SAM: 16000\n";
print "SBF: 01\n";
print "SSB: 16\n";
print "NCH: 1\n";
print "SPN: 3348\n";
print "LBD:\n";

my $basename = $ARGV[0] or die "usage: ./createpar.pl <basename of input files>";

# subtask 1: insert the orthographics with the ORT-Tag into the file
open (WORDS, '<', "$basename.wordlist");
my @words = <WORDS>;
close(WORDS);

my $i=0;
foreach my $word (@words)
{
	chomp $word; chomp $word;
	print "ORT:\t$i\t$word\n";
	$i++;
}
# subtask 2: insert the pronounciation with the KAN-Tag into the file
open (PRON,'<', "$basename.pronlist");
my @prons = <PRON>;
close(PRON);

my $j=0;
foreach my $pron (@prons)
{
	chomp $pron; chomp $pron;
	print "KAN:\t$j\t$pron\n";
	$j++;
}
