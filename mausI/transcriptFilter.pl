#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

require "Util.pm";

while (my $line = <STDIN>) {
	chomp $line;
	chomp $line;
	print Util::filterTranscript($line);
	print "$line\n";
}
