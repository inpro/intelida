#!/bin/perl

# editprons.pl 

# PSP08, A. Iwanow 

my $pronsfile = shift @ARGV;

open (ORTH,"<$pronsfile");

my @prons = <ORTH>;

# Entfernen unnötiger Elemente und Ausgabe für KANINV
my $kaninv='';
foreach my $el (@prons)
{
	$el=~s/[\+\n]//g;
	$kaninv.="$el\# ";
}

$kaninv=~s// /g;
$kaninv=~s/ :/:/g;
$kaninv=~s/[\s]+/ /g;
$kaninv=~s/.{3}$//;
$kaninv=~s/^ //;
print "a $kaninv";
