PSP 08, A. Iwanow

readme.txt

______________________________________________

Dateien:

wav2lab.sh	ausführendes Shellskript; Aufruf "./wav2lab_v2.sh file.wav file.ortho"

createlab.pl	Hilfsskript, das das Lab-Format erzeugt
createpar.pl	Hilfsskript, das eine par-Datei für den Input von MAUS erzeugt
editorth.pl	Hilfsskript, das den Text ausliest und Umlaute und das "SZ" konvertiert
getPron.pl	mitgeliefertes Skript, das den Text transkribiert
vm_ger.lex	mitgeliefertes Verbmobil-Lexikon, mit dem transkribiert wird

test1.lab	Output des Shellskripts (in der Trankription werden Pausen zwischen den Wörtern berücksichtigt)
test1.wav	Audio-Input	(Input für MAUS)
test1-orth.txt	Text-Input	(roher Input für MAUS, der von den Hilfsskripten bearbeitet wird)

readme.txt	Übersicht zum Umgang mit dem Archiv

______________________________________________

wav2lab.sh erzeugt mithilfe von MAUS und diversen Perlskripten ein Wavesurfer-Labeldatei (lab), die die Segmentgrenzen gesprochener Sprache in Wavesurfer darstellen.

Damit es läuft bitte den Pfad für MAUS im Shellskript anpassen.

Wenn die Datei, die zum Erzeugen der lab-Datei behalten werden sollen, dann bitte die Löschbefehle am Ende des Shellskripts auskommentieren. Um die temporären Dateien von MAUS zu behalten, bitte die entsprechenden Zeilen im Quellcode auskommentieren, und die darunterliegenden Zeilen einkommentieren:

	startmausGrid=$mauspath'/maus SIGNAL='$wavefile' BPF='$name'.par OUT='$name'.TextGrid OUTFORMAT=TextGrid'

______________________________________________

Die Testdateien lauten test1.wav und test1-orth.txt.

WAV: 	test1.wav
ORTH:	ich würde (w"urde) diesmal sagen
KAN:	Q'IC+ # v'Y6d@+ # d'i:sm"a:l+ # z'a:g@n			

TextGrid:	test1.TextGrid
lab:		test1.mau
______________________________________________
