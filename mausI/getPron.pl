#!/usr/bin/perl

# write pronunciations for given words on STDIN to STDOUT
# usage: echo "Hallo" | ./getPronunciation.pl vm_ger.lex

use strict;
use warnings;

# read dictionary
my $dictName = shift @ARGV or die "need lexicon to read as first argument!\n";
my $dict = readDict($dictName);

# process input 
my $line;
while (defined ($line = <STDIN>)) {
	chomp $line; chomp $line;
	my $ortho = $line;
	$ortho =~ s/\(\d+\)$//; # remove numbers in parentheses if there is one	
        my $origOrtho = $ortho;
	my $result = getPron($ortho);
	if (($result eq "") && ($ortho =~ m/^[A-ZÄÖÜ)]/)) {
		print STDERR "trying lowercase version for $ortho\n";
		$ortho = lc($ortho);
		$result = getPron($ortho);
	}
	if (($result eq "") && ($ortho =~ m/'$/)) {
		print STDERR "trying to chop off ' from $ortho\n";
		$ortho =~ s/'$//;
		$result = getPron($ortho);
	}
	if (($result eq "") && ($ortho =~ m/(^\-)|(\-$)/)) {
		print STDERR "trying to chop off - from $ortho\n";
		$ortho =~ s/^\-//;
		$ortho =~ s/\-$//;
		$result = getPron($ortho);
	}
	# give up:
	if ($result eq "") {
		warn "ERROR: Nothing found for $ortho!!\n";
		$result = "!!!NONE_FOUND!!!";
		`echo $origOrtho >> missingvocab`;
	}
	print "$result\n";
}

sub getPron {
	my $ortho = shift;
	return "" unless (defined $dict->{$ortho});
	my $result = @{$dict->{$ortho}}[0];
	print STDERR "discarding alternative pronunciations: "
			. (join ", ", @{$dict->{$ortho}}[1..$#{$dict->{$ortho}}]) . "\n"
		 if ($#{$dict->{$ortho}} > 0);
	return $result;
}

sub readDict {
	my $dictName = shift;
	open DICT, '<', $dictName;
	my %dict;
	my $line;
	while (defined ($line = <DICT>)) {
		chomp $line; chomp $line;
		next if ($line =~ m/^\s*$/);
		my @fields = split(/\s+/, $line);
		die "illegal format: $line\n" unless ($#fields == 1); # check format
		my ($ortho, $pron) = @fields;
		$ortho =~ s/\(\d+\)$//; # remove numbers in parentheses if there is one
		# create an empty list if we haven't seen this word yet
		$dict{$ortho} = () unless defined ($dict{$ortho});
		# add pronunciation to list of pronunciations for this word
		push @{$dict{$ortho}}, $pron;
	}
	close DICT;
	return \%dict;
}
