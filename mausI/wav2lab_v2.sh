#!/bin/sh

#-------------------------------------------------------------#
# PSP08						   A. Iwanow  #
#------------------------------------------------------------ #
# wav2lab_v2.sh runs maus and writes the output in a lab-file #
# output:						      #
# 		0.040000        0.200000        ich	      #
#		0.200000        0.510000        würde	      #
#		0.510000        1.050000        diesmal	      #
#		1.050000        1.920000        sagen	      #
#----------------------------------------------------------   #

# !!! PLEASE INSERT YOUR MAUS PATH HERE:
mauspath='/home/klette/Programme/maus'

# wav- and orth-file are arguments of wav2lab.sh
# usage: ./wav2lab.sh <WAV> <ORTHO>

# echo $1	# print out the script's arguments

wavefile=$1
orthfile=$2
# echo "wav: "$1"\n"	# print out the 1st argument, a wav-file
# echo "txt: "$2"\n"	# print out the 2nd argument, a orth-file 

name=${wavefile%%.wav}		# removes the filename expansion
# echo "name: "$name

# create pronounciation
# edit the orthographic file via perl script
perl editorth.pl $orthfile > $name.wordlist

# get pronounciations via script and dictionary
# by Timo
perl getPron.pl vm_ger.lex < $name.wordlist > $name.pronlist

# create par file
perl createpar.pl $name > $name'.par'

# start maus
startmausGrid=$mauspath'/maus SIGNAL='$wavefile' BPF='$name'.par OUT='$name'.TextGrid OUTFORMAT=TextGrid INSORTTEXTGRID=yes allowresamp=yes'
#startmausGrid=$mauspath'/maus SIGNAL='$wavefile' BPF='$name'.par OUT='$name'.TextGrid OUTFORMAT=TextGrid CLEAN=0'
$startmausGrid

gridfile=$name'.TextGrid'

perl createlab.pl $gridfile > $name'.lab'

#-------------------------------------------------------#
# removing the by-products
#rm $name.wordlist rm $name.pronlist rm $name'.par' rm $name'.TextGrid'

