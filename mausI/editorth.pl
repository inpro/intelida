#!/bin/perl

use strict;
use warnings;

# editoth.pl converts the items of the dictionary into valid maus input
# USAGE: perl editorth.pl $orthfile

# PSP08, A. Iwanow 

die "Please check the orthographic file!\n" unless (scalar @ARGV == 1);
my $orthfile = shift @ARGV;

open (ORTH,"<$orthfile");

my $orth = <ORTH>;

# Input: time\ttime\twords
# we only need the words
if ($orth =~/^.+?\t.+?\t(.+?)$/)
{
	$orth=$1;
} else {
	die "something doesn't match.\n"
}
# conversion 
$orth =~ s/ü/"u/g;
$orth =~ s/ä/"a/g;
$orth =~ s/ö/"o/g;
$orth =~ s/Ü/"U/g;
$orth =~ s/Ö/"O/g;
$orth =~ s/Ä/"A/g;
$orth =~ s/ß/"s/g;
$orth =~ s/[^A-Za-z"<->&]\s\$//g;

my @words = split(/\s/,$orth);
foreach my $el (@words)
{
	print "$el\n";
}
