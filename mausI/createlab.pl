#!/usr/bin/perl

use strict;
use warnings;

# createlan.pl converts a TextGrid into a wavessurfer label file
# usage: perl ./createlab.pl input.TextGrid

# PSP08, A. Iwanow 

my $gridfile = shift @ARGV;

#-----------------------------------------------------------------#
#--- 1st task: look for the ORT-tier within the TextGrid and 	--#
#--- push its content to @grid.	Extract the segments and its 	--#
#--- start and end time.					--#
#-----------------------------------------------------------------#

open (GRID, "<$gridfile");
my $var1='1';
my @grid = ();

# looks for the ORT-tier and pushes its elements to @grid
while (<GRID>)
{
	chomp;
	if ($var1=='1')	
	{

		if ($_=~/item \[1\]:/)	# ORT
		{
			$var1='0';
		}
	}
	elsif ($var1=='0')
	{
		if (/item \[2\]:/)	# MAU
		{
			$var1='1';
		}
		else
		{
			push @grid, $_;
		}
	}
}

# extractes each segment and its start and end time
my @elements = ();
foreach my $element (@grid)
{
	if ($element=~/xmin.+$/ || $element=~/xmax.+$/ || $element=~/text.+$/)
	{	
		push @elements,$element;
	}
}

shift @elements;			# perl criticized my slice-attempt
shift @elements;


#------------------------------------------------------------------#
#--- 2nd task: design of the lab-format ---------------------------#
#--- output detail:	0	0.040000			---#
#---		0.040000	0.200000	Ich		---#
#--- 		0.200000	0.510000	würde		---#
#--- 		0.510000	1.050000	diesmal		---#
#---		1.050000	1.920000	sagen		---#
#---		1.920000	2.030000			---#
#------------------------------------------------------------------#

my $von=-1;
my $bis=-1;
my $text='';
my @result=();

foreach my $element (@elements)
{
	if ($element=~/xmin = (.+)$/)
	{
		$von=$1 if ($1 > $von); 
	}
	if ($element=~/xmax = (.+)$/)
	{
		$bis=$1 if ($1 > $bis); 
	}
	if ($element=~/text = "(.*)"$/)
	{
		$text=$1;
		push @result,"$von\t$bis\t$text\n";
	}
}
print @result;
