#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";
require "Alignment.pm";

# usage: ./addWSAlignmentToTextGrid.pl textGrid.TextGrid newAlignment.lab { tiername { precedingTierName } }

package main;

my ($tgFileName, $alignmentFileName, $tierName, $precTierName) = @ARGV;

my $tg;
eval {
	$tg = TextGrid::newTextGridFromFile($tgFileName);
};
if ($@) {
	print "Could not open $tgFileName, creating new file instead\n";
	$tg = TextGrid::newEmptyTextGrid();
}
if ($tg->hasAlignment($tierName)) {
	warn "A tier named $tierName already exists. I'll replace it.\n";
	$tg->removeAlignmentByName($tierName);
}
my $alignment = Alignment::newAlignmentFromWavesurferFile($alignmentFileName);
$alignment->setName($tierName);
if (($precTierName) && ($tg->hasAlignment($precTierName))) {
	$tg->addAlignmentAfter($alignment, $precTierName);
} else {
	$tg->addAlignment($alignment);
}
#print $tg->toTextGridLines();
$tg->saveToTextGridFile($tgFileName);
