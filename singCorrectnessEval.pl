#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;
use Statistics::Lite qw(:all);
require "SingularAlignmentSequence.pm";
require "Alignment.pm";
require "Label.pm";

# split audio files into N equally sized bins, and calculate r-correctness,
# p-correctness and CR for each bin and the total.

# it should not be necessary to mess with this script (read as: do not touch!)

########## main program #############

package main;

# output format: 
print "# format: time, correctness, fair correctness, prefixcorrectness, old-style edit overhead, number of alignments evaluated, adds, subst, revokes, words\n";
# in old-style edit overhead there are no substitution. thus every substitution is counted twice, as one revoke and one add

# this is the area for experiment definitions

# you need to have all of the following variables defined:
# $smoothness: performs message smoothing on the hypotheses, set to 1 if you don't want message smoothing
# $epsilonHyp: only evaluate hypotheses at time t up to t-epsilonHyp
# $epsilonGold: evaluate gold standard up to time t-epsilonGold
# w_hyp_t-epsilonHyp_t == w_gold_t-epsilonGold

# both fair and strict correctness are always computed and output

# for smoothing you want variable $smoothness and $epsilonHyp=0,
# and either $epsilonGold=$epsilonHyp=0 or $epsilonGold=$smoothness*(-0.01)
# 

# FIXDED-LAG
# uncomment the following TWO lines for fixed-lag evaluation:
for (my $epsilonHyp = -0.1; $epsilonHyp <= 0.2; $epsilonHyp += 0.01 ) {
my $smoothness = 1;

# or:

# SMOOTHING
# uncomment the following THREE lines for message smoothing evaluation:
#for my $sequence (0..150) {
#for my $sequence (0..20) {
#my $smoothness = $sequence + 1;
#my $epsilonHyp = $sequence * (-0.01);

#my $smoothness = $sequence + 1;
#my $epsilonHyp = $sequence * (-0.01);




# you should not need to change anything below this point (except for bug-hunting and extensions)
#################################################################################################
# and if you do: hic sunt leones (actually, it's very easy to understand now...)

$epsilonHyp = sprintf("%.2f", $epsilonHyp);
print "$epsilonHyp  "; 

my $epsilonGold = $epsilonHyp;

my %results;

# main loop through the given arguments.
for my $file (@ARGV) {
	# read file
	my $seq = SingularAlignmentSequence::newSeqFromFile($file);
	my $duration = $seq->getDuration();

	$seq->smoothSequence($smoothness);
	$seq->cropSequence(); 
#print $seq->toString();
#	print STDERR "warning, running in NO-CROP-MODE";

	# read reference alignment
	my $refAlignment = $seq->getGold();
	my $totalAlignments = 0;
	my $prefixCorrectAlignments = 0;
	my $strictCorrectAlignments = 0;
	my $fairCorrectAlignments = 0;
	my $prevAlignment = $seq->getAlignmentAt(0);
	my $addIUs = $prevAlignment->getWords('remove'); # get the starting add()-IUs
	my $substIUs = 0;
	my $revokeIUs = 0;
	# before, this used to be a for loop through all alignments. this leads to overestimated correctness with filtered alignment sequences
	my $time = $seq->getFirstAlignment()->{time} - 0.01; # after cropping, this will start one alignment before the cropped time.
	$time -= $epsilonHyp if ($epsilonHyp < 0); # start later for negative epsilons, 
	my $runningDuration = $seq->getDuration();
	$runningDuration -= $epsilonHyp if ($epsilonHyp > 0); # stop earlier for positive epsilons
	while ($time <= $runningDuration) {
		my $alignment = $seq->getAlignmentAt($time);
		# statistics for edit messages
		my @IUs = $alignment->diffInIUEditsUpTo($prevAlignment, $time + $epsilonGold, 'remove', 'subst');
		$addIUs += scalar grep { $_->isAdd() } @IUs;
		$substIUs += scalar grep { $_->isSubst() } @IUs;
		$revokeIUs += scalar grep { $_->isRevoke() } @IUs;
		if ($#IUs >= 0) { $prevAlignment = $alignment->copyUpTo($time + $epsilonGold + 0.01); }
		# statistics for correctness
		$totalAlignments++;
		my $isPrefixCorrect = ($alignment->prefixUpTo($refAlignment, $time + $epsilonGold, 'lax'));
		my $isFairCorrect = ($refAlignment->equalsUpTo($alignment, $time + $epsilonGold, 'lax'));
		my $isStrictCorrect = ($refAlignment->equalsUpTo($alignment, $time, 'lax'));
		if ($isFairCorrect && !($isPrefixCorrect)) {
			print ("fair r-correct: " . ($isFairCorrect ? "true\n" : "false\n"));
			print ("p-correct: " . ($isPrefixCorrect ? "true\n" : "false\n"));
			print $alignment->toString();
			print $refAlignment->toString();
			print "in file $file at time $time with epsilonGold $epsilonGold and epsilonHyp? $epsilonHyp\n";
			die "THIS MUST NOT HAPPEN (if it's correct, then it must also be prefix-correct)";
		}
		$prefixCorrectAlignments++ if ($isPrefixCorrect);
		$fairCorrectAlignments++ if ($isFairCorrect);
		$strictCorrectAlignments++ if ($isStrictCorrect);
		$time = sprintf("%.2f", $time + 0.01);
	}
	my @IUs = $refAlignment->diffInIUEditsUpTo($prevAlignment, $time, 'remove', 'subst');
	$addIUs += scalar grep { $_->isAdd() } @IUs;
	$substIUs += scalar grep { $_->isSubst() } @IUs;
	$revokeIUs += scalar grep { $_->isRevoke() } @IUs;
	$results{addIUs} += $addIUs;
	$results{substIUs} += $substIUs;
	$results{revokeIUs} += $revokeIUs;
	$results{changes} += $addIUs + 2 * $substIUs + $revokeIUs;

	$results{words} += scalar $refAlignment->getWords('remove');

	$results{total} += $totalAlignments;
	$results{strictCorrect} += $strictCorrectAlignments;
	$results{fairCorrect} += $fairCorrectAlignments;
	$results{prefixCorrect} += $prefixCorrectAlignments;
}

my $words = $results{words};
my $changes = $results{changes}; 
my $total = $results{total};
die "fewer changes ($changes) than words ($words) is impossible (should be at least)" if ($changes < $words);
print	(($results{strictCorrect} / $total), " ");
print	(($results{fairCorrect} / $total), " ");
print	(($results{prefixCorrect} / $total), " ");
print	((($changes - $words) / $changes), " ");
print	$total, " ", $results{addIUs}, " ", $results{substIUs}, " ", $results{revokeIUs}, " ", $words;
print   "\n";

#print "$changes changes for $words words, substitutions are counted twice;";
#print "$results{addIUs} addIUs, $results{substIUs} substIUs, $results{revokeIUs} revokeIUs\n";

}
