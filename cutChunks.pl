#!/usr/bin/perl

#Copyright (C) 2008 Timo Baumann, Janine Wolf
#This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Audio::Wav;
use Audio::Wav::Read;
use Audio::Wav::Write;
use Data::Dumper;
use POSIX;

# hint: you may want to fade into and out of audio with sox: 
# sox $file /tmp/$file fade 0.03 0
# for file in *.wav; do sox $file /tmp/$file fade 0.03 0; mv /tmp/$file .; done
#
# WARNING: there seems to be a memory leak in Audio::Wav.
# do not run with too many files at a time (better restart every once in a while)


# usage: ./cutFiles.pl < fileBasename1 fileBasename2 ...
# ls *.wav | sed 's/.wav//' | ../../cutFiles.pl

#
# PARAMETERS (optional):
# maximum and minimum silence duration as well as maximum overlap can be given as parameters (in this order)


my $OUTPUT_PATH = "CutFiles/";
mkdir $OUTPUT_PATH unless (-e $OUTPUT_PATH); # creates directory for output files
my $INPUT_PATH = "wav/";
my $LABEL_PATH = "chunklevelannotation/";
my $LOG_PATH = "logs/";
mkdir $LOG_PATH unless (-e $LOG_PATH);

my $MAX_SILENCE_DURATION = (@ARGV) ? shift @ARGV : 0.5;
my $MIN_SILENCE_DURATION = (@ARGV) ? shift @ARGV : 0.2;
my $MAX_OVERLAP = (@ARGV) ? shift @ARGV : 0.1;

print STDERR <<"EOF";
max silence duration: $MAX_SILENCE_DURATION
min silence duration: $MIN_SILENCE_DURATION
max overlap: $MAX_OVERLAP
EOF


#Leading+Trailing Silence --> NACHFRAGEN!!!
#
# sagen wir mal: max(0.5 Sekunden, vorhandene Stille)
# jeweils für leading und trailing silence

my $audio_details;
my $inAudio;
my $overallCount = 0; # counts written files
my $overallDuration = 0;

my $unitCount = 0; # counts units

my $lastUnitEnd; # global scope to be accessible when dieing
my $unitEnd; # global scope to be accessible when dieing

while (my $fileID = <STDIN>) 
{
	chomp $fileID; chomp $fileID;
	
	print STDERR "working on file $fileID\n";
	$inAudio = openWav("$INPUT_PATH/$fileID.wav");
	
	open LOG, '>', "$LOG_PATH/$fileID.log";
	print LOG "count\tfilename\tstartTime\tendTime\tduration\n";
	
	my @units = readUnits("$LABEL_PATH/$fileID.ortho");
	
	$lastUnitEnd = 0;	

	for (my $i = 0; $i <= $#units; $i++) {
		my $unit = $units[$i];
		
		my $unitStart = $unit->{'start'};
		$unitEnd = $unit->{'end'};
		my $unitDuration = $unitEnd - $unitStart;

		my $leadingSilence = $unitStart - $lastUnitEnd;
		$leadingSilence = cropSilence($leadingSilence);
		if ($leadingSilence < 0) { die "oups"; }
		my $nextUnitStart = ($i < $#units) ? $units[$i + 1]->{'start'} : $inAudio->length_seconds();
		my $trailingSilence = $nextUnitStart - $unitEnd;
		$trailingSilence = cropSilence($trailingSilence);
		if (($trailingSilence < $MIN_SILENCE_DURATION)
		&& ($i < $#units) # it's perfectly alright for the ultimate label to not have trailing silence
		){
			warn "the silence is very short ($trailingSilence). I will merge this and the next unit.\n";
			warn "$lastUnitEnd, $unitEnd";
			$units[$i+1] = mergeUnits($unit, $units[$i+1]);
			
			next;
#			die "this silence is too short: $trailingSilence + $MAX_OVERLAP < 2 * $MIN_SILENCE_DURATION around time $lastUnitEnd, $unitEnd";	
		}

		my $outName = newOutName($fileID, $unitCount);
		
		writeUnit($unitStart - $leadingSilence, $unitEnd + $trailingSilence, $outName);
		
		$unit->{'start'} = $leadingSilence;
		$unit->{'end'} = $leadingSilence + $unitDuration;
#		print STDERR "start: $unit->{'start'},\tend: $unit->{'end'}.\n";
		
		writeLabels($outName, $unit);
#		print STDERR "$outName\t$unit->{'label'}\n";
		
		logUnit($unitCount, $outName, $unitStart - $leadingSilence, $unitEnd + $trailingSilence, $unitDuration);
		
		$lastUnitEnd = $unitEnd;
		$unitCount++;
		$overallDuration += $unitDuration;
		
	}
	$overallCount += $unitCount;
	$unitCount = 0;
	
}
print STDERR "Done. $overallCount files have been written, containing $overallDuration seconds of speech.\n";

# crop a silence so that it does not overlap with a previous nor following silence
sub cropSilence {
	my $silence = shift;
	if ($silence + $MAX_OVERLAP >= 2 * $MAX_SILENCE_DURATION) { 
		$silence = $MAX_SILENCE_DURATION;
	} else {
		$silence = ($silence + $MAX_OVERLAP) / 2;
	}
	return $silence;
}

sub mergeUnits {
	my $unit1 = shift;
	my $unit2 = shift;
	return { 'start' => $unit1->{'start'}, 'end' => $unit2->{'end'}, 'label' => $unit1->{'label'} . ' ' . $unit2->{'label'} };
}

sub writeUnit {
	my $startTime = shift;
	my $endTime = shift;
	my $duration = $endTime - $startTime;
	my $filename = shift;
	$filename .= '.wav';
	# write audio file: 
	# move to the start in inAudio
	$inAudio->move_to_sample(time2sample($startTime));
	# read data from inAudio into buffer
	my $buffer = $inAudio->read_raw_samples(time2sample($duration));
	# open outAudio
	my $wav = Audio::Wav::new();
	my $outAudio = $wav->Audio::Wav::write("$OUTPUT_PATH$filename", $audio_details);
	# write data from buffer to outAudio
	$outAudio->write_raw_samples($buffer);
	# close outAudio
	$outAudio->finish();
}

sub writeLabels {
	my $filename = $OUTPUT_PATH . shift;
	$filename .= '.ortho';
	open ORTHO, '>', $filename;
	while (my $label = (shift)) 
	{
		print ORTHO $label->{'start'} . "\t" . $label->{'end'} . "\t" . $label->{'label'} . "\n";
	}
	
	close ORTHO;
}

sub time2sample {
	my $time = shift;
	my $samplingRate = $audio_details->{'sample_rate'};
	return POSIX::floor($time * $samplingRate);
}

sub newOutName {
	return (shift) . "_Unit_". (shift);
}

sub openWav {
	my $filename = shift;
	my $wav = Audio::Wav::new();
	my $inAudio = $wav->Audio::Wav::read("$filename") or die "Could not open wavefile $filename\n";
	$audio_details = $inAudio->details();
#	die "Wrong number of channels in $filename\n" unless ($audio_details{'channels'} == 1);
	return $inAudio;
}

# version for wavesurfer-files
sub readUnits {
	my $filename = shift;
	open ORTHO, '<', $filename or die "could not open $filename\n";
	# skip over header
	my @units = ();
	while (my $line = <ORTHO>) {
		$line =~ m/\s*([\d]+.?[\d]*)\s+([\d]+.?[\d]*)\s+(.+)/ or die "cannot parse format: $line\n";
		my ($start, $end, $label) = ($1, $2, $3);
		if ($label !~ /^<[^>]+>$/ 
		 && $label !~ /^\s*$/) # mostly this should cover sil, trash, side-talk, slide, lachen, schmunzeln, ..., but only if they are the only content of a chunk
		{
			push @units, {'start' => $start, 'end' => $end, 'label' => $label};
		}
	}
	return @units;
}

# writes Logfile incrementally: store information about number of unit and offset (overall)
sub logUnit
{
  my $counter = shift;
  my $filename = shift;
  my $offset_time = shift;
  my $end_time = shift;
  my $duration = shift;
 
  print LOG "$counter\t$filename\t$offset_time\t$end_time\t$duration\n";
}


