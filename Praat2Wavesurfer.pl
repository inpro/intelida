#!/usr/bin/perl -w

# script for converting specific Praat-Textgrid-tiers to Wavesurfer ortho-files   
# by Janine Wolf, Anna Iwanow, Timo Baumann

use strict;
use Switch;

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use TextGrid;


# This is the list of items to be converted. 
# There will be one tier per participant.
# The resulting ortho file names are dependent on the corpus:
# -In case of a single speaker it will be "fileID.ortho";
# -In case of two speakers, it will be "fileID_SpeakerID.ortho"
#  See the documentation for details.

# usage:
# specify the type of corpus and the file names to be converted
# example: perl Praat2Wavesurfer.pl dual1 pair1.TextGrid pair2.TextGrid


# case switch in order to differentiate between corpora 
my @items = ();
my @file_marker =();
my $ID = shift @ARGV;

# E: ehemals Executor/ Instruction Follwer
# I: ehemals Instructor/ Instruction Giver
switch ($ID) {
  case "single1" {@items = ("userTranscript")		} # Greifarm_WOZ, WOZ
  case "single2" {@items = ("utterances")	    	} # Pento_Naming
  case "dual1"   {@items = ("I-utts", "E-utts");
		  @file_marker = ("I", "E")		} # P2T
  case "dual2"   {@items = ("E-utts", "I-utts"); 
                  @file_marker = ("E", "I")      	} # Visual_Pento
  else           {print "ID für Tiernamen unbekannt"}
}


# file prefix; e.g. "PREFIX.TextGrid"
my $fileID;
# name for wavesurfer output file
my $orthofilename;


# process all arguments (i.e. all files)
foreach my $file (@ARGV) 
{
	# extract file ID
	if ($file =~ /(.*).TextGrid/) 
	{
		$fileID = $1;
	}
    
  
	# read input TextGrid
	my $tg = TextGrid::newTextGridFromFile($file);

	
	# current array position in @items (and therefore position in @file_marker as well)
	my $relationPointer = 0;
	# unit counter
	my $noOfUnits = 0;
	
	# process all items in the list above
	foreach my $cur_item (@items) {

	 # check whether the current tier exists
	 if($tg->hasAlignment($cur_item))
	 {
		# specify filename for output
		if ($cur_item =~ m/utterances|userTranscript/) 
		{
			$orthofilename = $fileID.".ortho";
			open ORTHO_OUT, ">", "$orthofilename";

		} 
		else 
		{
			$orthofilename = $fileID."_$file_marker[$relationPointer].ortho";
			open ORTHO_OUT, ">", "$orthofilename";
		}

		# read tier contents
		my $cur_alignment = $tg->getAlignmentByName($cur_item);

		# convert and write data
		foreach my $label ($cur_alignment->getLabels())
		{
			# ignore labels that only contain one marker and nothing else
			next if ($label->{text} =~ m/^<[^>]*>$/);
			print ORTHO_OUT $label->toWavesurferLine();
			$noOfUnits++;
		}
	
	 print STDERR "processed tier $cur_item with $noOfUnits units.\n";	
	 close ORTHO_OUT;
	 
	 # proceed to next item
	 $relationPointer++;
	 
	 
	 } # end: if alignment exists
	 else {print STDERR "tier not found: $cur_item\n"};

	 } # end: foreach item   	
    	
 } # end: foreach file   	
