#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;

require "Lexicon.pm";

#package INshell;
#require Term::Shell;
#use base qw(Term::Shell);

# read in lexicon in Sphinx format:
my $lex = Lexicon::newFromFile("dsg.lex.noalternatives");
$lex->instantiateCanonicalPronunciations();
#map { $_->removePhoneFromPronunciations("Q") } values %{$lex->{entries}};
#map { $_->replacePhoneInPronunciations("e", "e:") } values %{$lex->{entries}};
#map { $_->replacePhoneInPronunciations("E:", "E") } values %{$lex->{entries}};
#map { $_->replacePhoneInPronunciations("i", "i:") } values %{$lex->{entries}};
#map { $_->replacePhoneInPronunciations("o", "o:") } values %{$lex->{entries}};
#map { $_->replacePhoneInPronunciations("u", "u:") } values %{$lex->{entries}};
#map { $_->{canonic} = join " ", @{$_->{instances}->[0]} } values %{$lex->{entries}};
my %phoneset = $lex->toPhoneset();
#print join "\n", keys %phoneset;
foreach my $key (sort keys %phoneset) {
	print "$key\t" . $phoneset{$key} . "\n";
}
#$lex->outputByLexicalOrder();

