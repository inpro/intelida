	_____________________________________________________________
	  _                                         _           _   
	 (_)_ __  _ __  _ __ ___    _ __  _ __ ___ (_) ___  ___| |_ 
	 | | '_ \| '_ \| '__/ _ \  | '_ \| '__/ _ \| |/ _ \/ __| __|
	 | | | | | |_) | | | (_) | | |_) | | | (_) | |  __/ (__| |_ 
	 |_|_| |_| .__/|_|  \___/  | .__/|_|  \___// |\___|\___|\__|
	         |_|               |_|           |__/               
	_____________________________________________________________

InTELiDa data format
====================


The main data format supported (and crucial for incrementality) is the 
`inc_reco` file format for storing incremental timed linguistic data.

## inc_reco description

The `inc_reco` format contains one or more timed **alignments** which describe the 
state of affairs (e.g. what a speech recognizer thinks about what is being 
spoken) at successive moments in time. Alignments themselves consist of one or 
more **label lines** which must be temporally ordered and should not overlap.
Some labels (such as `<s>`, `<sil>` and maybe more) are handled in particular 
ways (mostly in `Label::isSilence`, maybe in other places as well).

The following grammar describes the format: 

	INC_RECO   = ( ALIGNMENT )+ [ GOLD_ALIGN ]
	ALIGNMENT  = 
				Time: TIME { <tab> ADD_INFO } <newline>
				( LABEL_LINE )+
				<empty_line>
	GOLD_ALIGN = 
				Time: gold { <tab> ADD_INFO } <newline>
				( LABEL_LINE )+
				<empty_line>
	TIME       = \d*.\d* # standard interpretation is seconds, but could be anything
	LABEL_LINE = START <tab> END <tab> LABEL_TEXT { <tab> ADD_INFO } <newline>
	START      = TIME
	END        = TIME
	LABEL_TEXT = any text you want without newlines or tabs
	ADD_INFO   = KEY=VALUE [, ADD_INFO] # you may put as many key-value pairs as you please
	KEY        = KEY_VALUE
	VALUE      = KEY_VALUE
	KEY_VALUE  = any text you want without newlines, tabs, equal signs or commas

Conforming inc_reco files should have their alignments temporally ordered, 
every alignment should be non-empty and ordering should be strictly monotonic. 
The last alignment may be followed by a gold-standard alignment that will be 
used for evaluating the timing and for computing word error rates. 

To allow for n-best hypotheses, the `nbinc_reco` file format orders alignments 
only monotonically (i.e., consecutive alignments in the file may have the same 
timing). Alignments with the same time are taken to be n-best hypotheses, 
conventionally ordered by quality. 
