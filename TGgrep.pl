#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;

require "TextGrid.pm";
require "Util.pm";

package main;

my $usage = "usage: TGgrep.pl [-t tierregexp] <regexp> TextGrids\n";

my $tierRegexp = "";
unless (@ARGV) {
	print $usage ;
	exit;
}
if ($ARGV[0] eq '-t') {
	shift @ARGV;
	$tierRegexp = shift @ARGV;
}
my ($regexp, @files) = @ARGV or die $usage;

for my $file (@files) {
	my $tg = TextGrid::newTextGridFromFile($file);
	foreach my $tier ($tg->getAlignmentsByName($tierRegexp)) {
		my $tiername = $tier->getName();
		my $output = join "", 
			map { $_->toWavesurferLine() } 
				grep { $_->{text} =~ m/$regexp/ } 
					$tier->getLabels();
		if ($output) {
			print "$file \@ $tiername :\n";
			print $output;
		}
	}

}
