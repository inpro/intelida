#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/

use strict;
use warnings;
use Statistics::Lite qw(:all);
require "SingularAlignmentSequence.pm";
require "Alignment.pm";
require "Label.pm";
require "Util.pm";

##
## plan for the overhaul:
## OK - create getFRDelays() and getLWDelays() in SingularAlignmentSequence
##   - rework code while doing it
## - create some simple enough tests
## - check with one more complex test
## - make sure results differ from this only if that makes sense
## 



# combines the functionality of delay_frEval.pl and delay_lwEval.pl

# measures calculated:
#
# when a word was first rightly recognized (delay_{fr})
# when a word was last wrongly recognized (delay_{lw})
# additionally computes the "correction time", which is the difference
# between delay_fr and delay_lw
# and shows the distribution of correction times

#
# silence markers are skipped in evaluation! (they're not words after all)
# 
# see also delay_lwEval.pl and delay_frEval.pl

# usage:
# ./timingEval.pl data/*.LM8.inc_reco

########## main program #############

package main;

sub printAllStats {
	my %delay_hash = @_;
	for my $key ('from_start', 'to_end', 'word_rel') {
		print "\n$key:\n";
		printStats(@{$delay_hash{$key}});
	}
#	print STDERR "\nfor from_start: ";
#	print STDERR join ", ", @{$delay_hash{from_start}};
#	print STDERR "\n";
}

sub printStats {
	my @data = @_;
	@data = sort {$a <=> $b} @data;
	print "count:   ", count(@data);
	print "\nmean:   ", mean(@data);
	print "\nstddev: ", stddev(@data);
	print "\nmedian: ", median(@data);
	print "\nmin:    ", min(@data);
	print "\nmax:    ", max(@data);
	print "\n25%:    ", $data[int($#data *.25)];
	print "\n75%:    ", $data[int($#data *.75)];
	print "\nrange:  ", range(@data);
	print "\n";
}


####### START MAIN #######

my %delay_fr = ( from_start => [], to_end => [], word_rel => [] ); # in the paper: WFC
my %delay_lw = ( from_start => [], to_end => [], word_rel => [] ); # in the paper: WFF

# main loop through the given arguments.
for my $file (@ARGV) {
#	print STDERR "$file\n";
	# read file
	my $seq = SingularAlignmentSequence::newSeqFromFile($file);
	# there is no need to crop as we are just dealing with hyp-changes
	# then again, it doesn't hurt results and should slightly improve performance
#	$seq->makeContinuous();
#	$seq->cropSeq();

########## calculations for delay_fr ##########
	my %file_delay_fr = $seq->getFODelays();
	push @{$delay_fr{from_start}}, @{$file_delay_fr{from_start}};
	push @{$delay_fr{to_end}}, @{$file_delay_fr{to_end}};
	push @{$delay_fr{word_rel}}, @{$file_delay_fr{word_rel}};

########## calculations for delay_lw ##########

	my %file_delay_lw = $seq->getFDDelays();
	push @{$delay_lw{from_start}}, @{$file_delay_lw{from_start}};
	push @{$delay_lw{to_end}}, @{$file_delay_lw{to_end}};
	push @{$delay_lw{word_rel}}, @{$file_delay_lw{word_rel}};

}

print "results from singTimingEval.pl\n\n";

print "stats for FO \n";
printAllStats(%delay_fr);

print "\n\nstats for FD \n";
printAllStats(%delay_lw);

# finally compute the correction time for each word, which is
# the difference between first right and last wrong 
# (with a common anchor, either from_start or to_end)
my @correctionTimes;

my $immediatelyCorrect = 0; # number of words that were right from the beginning

my @fr_from_start = @{$delay_fr{from_start}};
my @lw_from_start = @{$delay_lw{from_start}};
my $maxCount = $#fr_from_start + 1;
if ($#fr_from_start != $#lw_from_start) {
	print STDERR "$#fr_from_start, $#lw_from_start\n";
	die "\@fr_from_start and \@lw_from_start must be of equal length!\n";
}
for my $i (0..$#fr_from_start) {
	my $correctionTime = $lw_from_start[$i] - $fr_from_start[$i];
	if ($correctionTime > 0.0005) { # allow for floating point imprecision
		push @correctionTimes, $correctionTime;
	} else {
		$immediatelyCorrect++;
	}
}

print "\n\npercentage immediately correct: " . $immediatelyCorrect / $maxCount;

print "\n\nstats for correction times (iff corrections were necessary): \n\n";
printStats(@correctionTimes);

#print STDERR join ", ", @correctionTimes;
#print STDERR "\n";

print "\n#data to plot integral of correction time distribution\n";
print "#this can be used to estimate confidence for a word hypothesis to not change anymore\n";
print "# format: time (in seconds), percentage of correciton times = time, percentage of correction times <= time\n";
@correctionTimes = sort { $a <=> $b } @correctionTimes;
my $globalCount = $immediatelyCorrect;
my $maxTime = max(@correctionTimes);
die "no maxTime!" unless (defined $maxTime);
for (my $time = 0.0; $time <= $maxTime; $time += 0.01) {
#	print sprintf("%.2f", $time);
#	print "\t";
	my $count = 0;
	while ($correctionTimes[0] <= $time) {
		shift @correctionTimes;
		$count++;
	}
	$globalCount += $count;
#	print $count / $maxCount . "\t" . $globalCount / $maxCount;
#	print "\n";
}
# print last line of distribution
print sprintf("%.2f", $maxTime) . "\t" . 1 / $maxCount . "\t1.0\n";

print STDERR mean(@{$delay_fr{from_start}}) . "\t" . mean(@{$delay_lw{to_end}}) . "\t" . $immediatelyCorrect / $maxCount . "\n";


