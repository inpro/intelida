#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib"; # import libraries from lib/
require "Lexicon.pm";
use utf8;

use constant skipAccents => (1 == 1); # false
use constant skipGlottalStop => (0 == 1); # false
use constant skipSylBounds => (0 == 1); # false
use constant convertVmToUnicode => (0 == 0); # true

my ($lexname) = @ARGV;

my $lex = Lexicon::newFromFile($lexname);
foreach my $entry (values %{$lex->{entries}}) {
	$entry->vmToUnicode() if (convertVmToUnicode);
	$entry->postProcessSampa(skipAccents, skipGlottalStop, skipSylBounds);
}
$lex->outputByLexicalOrder();
