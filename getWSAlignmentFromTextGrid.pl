#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/lib";

use strict;
use warnings;
require "TextGrid.pm";

# extract a tier from a TextGrid and print it to STDOUT in wavesurfer format

package main;

$#ARGV == 1 or die "need two arguments: filename of textgrid and name of tier";
my ($filename, $tiername) = @ARGV;

my $tg = TextGrid::newTextGridFromFile($filename);
my $alignment = $tg->getAlignmentByName($tiername);
print $alignment->toWavesurferLines();
